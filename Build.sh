#!/bin/bash
if [[ $1 = "--Debug" || $1 = "-D" ]] 
then
    if [[ $2 = "--build" || $2 = "-b" ]] 
    then 
        cmake --build cmake-build-debug/ --target $3 -- -j 8
    elif [[ $2 = "--run" || $2 = "-r" ]]
    then
        ./cmake-build-debug/$3 $4 $5 $6 $7
    elif [[ $2 = "clean" || $2 = "-c" ]]
    then
        rm -rf cmake-build-debug
    else
        mkdir cmake-build-debug
        cd cmake-build-debug
        cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles" ../
    fi
elif [[ $1 = "--Release" || $1 = "-R" ]]
then
    if [[ $2 = "build" || $2 = "-b" ]] 
    then 
        cmake --build cmake-build-release/ --target $3 -- -j 8
    elif [[ $2 = "run"  || $2 = "-r"  ]]
    then
        ./cmake-build-release/$3 $4 $5 $6 $7
    elif [[ $2 = "clean" || $2 = "-c" ]]
    then
        rm -rf cmake-build-release
    else
        mkdir cmake-build-release
        cd cmake-build-release
        cmake -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - Unix Makefiles" ../
    fi
fi

