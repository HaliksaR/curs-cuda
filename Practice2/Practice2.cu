#include <cstdlib>
#include <curand.h>
#include <cublas_v2.h>
#include <iostream>
#include <cuda.h>
#include <random>

#define BLOCK_SIZE 64

__global__ void matrix_shared(const float *a, const float *b, float *c, int N);

void fillRand(float *matrix, int N);

void print_matrix(const float *matrix, int N);

void GPU_matrix(int N, const float *hostA, const float *hostB);

void CPU_matrix(int N, const float *hostA, const float *hostB);

void fillRand(float *matrix, int N) {
    std::random_device randomDevice{};
    std::mt19937 engine{randomDevice()};
    std::uniform_real_distribution<float> distribution(0, 100);
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < count; ++j) {
            pointer[i * count + j] = distribution(engine);
        }
    }
}

void print_matrix(const float *matrix, int N) {
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << matrix[j * N + i] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

__global__ void matrix_shared(const float *a, const float *b, float *c, const int N) {
    unsigned int row = blockIdx.y * BLOCK_SIZE + threadIdx.y;
    unsigned int col = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    __shared__ float tileA[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ float tileB[BLOCK_SIZE][BLOCK_SIZE];

    float temp = 0;
    unsigned int idx;

    for (int i = 0; i < gridDim.x; ++i) {
        idx = row * N + i * BLOCK_SIZE + threadIdx.x;
        if (idx >= N * N) {
            // N не может делиться на BLOCK_SIZE
            tileA[threadIdx.y][threadIdx.x] = 0;
        } else {
            tileA[threadIdx.y][threadIdx.x] = a[idx];
        }

        idx = (i * BLOCK_SIZE + threadIdx.y) * N + col;
        if (idx >= N * N) {
            tileB[threadIdx.y][threadIdx.x] = 0;
        } else {
            tileB[threadIdx.y][threadIdx.x] = b[idx];
        }
        __syncthreads();

        for (int k = 0; k < BLOCK_SIZE; ++k) {
            temp += tileA[threadIdx.y][k] * tileB[k][threadIdx.x];
        }
        __syncthreads();
    }
    if (row < N && col < N) {
        c[row * N + col] = temp;
    }
}

void GPU_matrix(int N, const float *hostA, const float *hostB) {
    float *hostC;
    cudaMallocHost((void **) &hostC, N * N * sizeof(float));

    float gpu_elapsed_time_ms;

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, nullptr);

    float *deviceA, *deviceB, *deviceC;
    cudaMalloc((void **) &deviceA, N * N * sizeof(float));
    cudaMalloc((void **) &deviceB, N * N * sizeof(float));
    cudaMalloc((void **) &deviceC, N * N * sizeof(float));

    cudaMemset((void **) deviceC, 0, N * N * sizeof(float));

    cudaMemcpy(deviceA, hostA, N * N * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(deviceB, hostB, N * N * sizeof(float), cudaMemcpyHostToDevice);

    unsigned int grid_rows = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;
    unsigned int grid_cols = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

    dim3 dimGrid(grid_cols, grid_rows);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);


    matrix_shared<<<dimGrid, dimBlock>>>(deviceA, deviceB, deviceC, N);
    cudaDeviceSynchronize();

    cudaMemcpy(hostC, deviceC, N * N * sizeof(float), cudaMemcpyDeviceToHost);

    cudaEventRecord(stop, nullptr);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&gpu_elapsed_time_ms, start, stop);

#ifdef ENABLE_DEBUG_MACRO
    std::cout << "GPU C =" << std::endl;
    print_matrix(hostC, N);
#endif

    std::cout << "Time elapsed on matrix multiplication of "
              << N << "x" << N << " . " << N << "x" << N
              << " on GPU: " << gpu_elapsed_time_ms << " ms." << std::endl;

    cudaFree(deviceA);
    cudaFree(deviceB);
    cudaFree(deviceC);
    cudaFree(hostC);
}

void CPU_matrix(int N, const float *hostA, const float *hostB) {
    auto *hostC = (float *) malloc(N * N * sizeof(float));

    float cpu_elapsed_time_ms;

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, nullptr);

    int i, j, h;
    for (i = 0; i < N; ++i) {
        for (j = 0; j < N; ++j) {
            float tmp = 0.0;
            for (h = 0; h < N; ++h) {
                tmp += hostA[i * N + h] * hostB[h * N + j];
            }
            hostC[i * N + j] = tmp;
        }
    }

    cudaEventRecord(stop, nullptr);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&cpu_elapsed_time_ms, start, stop);

#ifdef ENABLE_DEBUG_MACRO
    std::cout << "CPU C =" << std::endl;
    print_matrix(hostC, N);
#endif
    std::cout << "Time elapsed on matrix multiplication of "
              << N << "x" << N << " . " << N << "x" << N
              << " on CPU: " << cpu_elapsed_time_ms << " ms." << std::endl;
    free(hostC);
}

int main() {
    int N = 2048;
    auto *hostA = (float *) malloc(N * N * sizeof(float));
    auto *hostB = (float *) malloc(N * N * sizeof(float));
    fillRand(hostA, N);
    fillRand(hostB, N);
#ifdef ENABLE_DEBUG_MACRO
    std::cout << "A =" << std::endl;
    print_matrix(hostA, N);

    std::cout << "B =" << std::endl;
    print_matrix(hostB, N);
#endif
    GPU_matrix(N, hostA, hostB);

    CPU_matrix(N, hostA, hostB);

    free(hostA);
    free(hostB);
    return 0;
}