#include <cuda.h>
#include "Utils.h"
#include<cuda_profiler_api.h>

__global__ void
GPU_gauss_up(float *matrixA, float *vectorB, const int size, const int position) {
    unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (position == x && position > y) {
        vectorB[y] -= vectorB[position] * (matrixA[y * size + position] / matrixA[position * size + position]);
        matrixA[y * size + x] = 0;
    }
}

__global__ void
GPU_gauss_down(float *matrixA, float *vectorB, const int size, const int position) {
    unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (position < x && position < y) {
        float d = matrixA[y * size + position] / matrixA[position * size + position];
        if (position == x)
            vectorB[y] -= vectorB[position] * d;
        matrixA[y * size + x] -= d * matrixA[position * size + x];
    }
}


__global__ void
GPU_gauss_up_shared(float *matrixA, float *vectorB, const int size, const int position) {
    unsigned int y = blockIdx.y * BLOCK_SIZE + threadIdx.y;
    unsigned int x = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    __shared__ float tileA[BLOCK_SIZE * BLOCK_SIZE];
    __shared__ float tileB[BLOCK_SIZE];
    __shared__ float division;

    for (unsigned int index = y; index < y + size; index += blockDim.x) {
        tileA[threadIdx.y * size + threadIdx.x] = matrixA[index + threadIdx.y * size + threadIdx.x];

        tileB[y] = vectorB[y];
        division = matrixA[y * size + position] / matrixA[position * size + position];
        __syncthreads();

        if (position == x && position > y) {
            tileB[y] -= tileB[position] * division;
            tileA[y * size + x] = 0.0f;
        }
        __syncthreads();

        matrixA[index + threadIdx.y * size + threadIdx.x] = tileA[y * size + x];
        vectorB[y] = tileB[y];
    }
}

__global__ void
GPU_gauss_down_shared(float *matrixA, float *vectorB, const int size, const int position) {
    unsigned int y = blockIdx.y * BLOCK_SIZE + threadIdx.y;
    unsigned int x = blockIdx.x * BLOCK_SIZE + threadIdx.x;

    __shared__ float tileA[BLOCK_SIZE * BLOCK_SIZE];
    __shared__ float tileB[BLOCK_SIZE];
    __shared__ float division;

    for (unsigned int index = y; index < y + size; index += blockDim.x) {

        tileA[threadIdx.y * size + threadIdx.x] = matrixA[index + threadIdx.y * size + threadIdx.x];
        tileB[y] = vectorB[y];
        division = matrixA[y * size + position] / matrixA[position * size + position];
        __syncthreads();

        if (position < x && position < y) {
            if (position == x) tileB[y] -= tileB[position] * division;
            tileA[threadIdx.y * size + x] -= division * tileA[position * size + x];
        }
        __syncthreads();

        matrixA[index + threadIdx.y * size + threadIdx.x] = tileA[y * size + x];
        vectorB[y] = tileB[y];
    }
}

void
kernel(ModeMemory const &mode, int const &size, TimeMode const timeMode) {
    cudaProfilerStart();
    float allTime = 0;
    float copyToCPUTime = 0;
    float runGPUTime = 0;
    float copyToGPUTime = 0;
    float initGPUTime = 0;
    float fillTime = 0;
    float initCPUTime = 0;

    cudaEvent_t allStartCudaEvent, allEndCudaEvent, startCudaEvent, endCudaEvent;

    cudaEventCreate(&allStartCudaEvent);
    cudaEventCreate(&allEndCudaEvent);
    cudaEventCreate(&startCudaEvent);
    cudaEventCreate(&endCudaEvent);

    float *hostMatrixA, *hostVectorB, *hostVectorC;
    float *deviceMatrixA, *deviceVectorB;

    cudaEventRecord(allStartCudaEvent, nullptr);
    { // allTime
        cudaEventRecord(startCudaEvent, nullptr);
        {  // initCPUTime
            hostMatrixA = (float *) malloc(size * size * sizeof(float));
            hostVectorB = (float *) malloc(size * sizeof(float));
            hostVectorC = (float *) malloc(size * sizeof(float));
        }
        Utils::Time::recorded(&initCPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // startCudaEvent
            Utils::Fill::CPU_fill_matrix_rand(hostMatrixA, size, RAND_DISTRIBUTION_MIN, RAND_DISTRIBUTION_MAX);
            Utils::Fill::CPU_fill_vector_rand(hostVectorB, size, RAND_DISTRIBUTION_MIN, RAND_DISTRIBUTION_MAX);
        }
        Utils::Time::recorded(&fillTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // initGPUTime
            cudaMalloc((void **) &deviceMatrixA, size * size * sizeof(float));
            cudaMalloc((void **) &deviceVectorB, size * sizeof(float));
        }
        Utils::Time::recorded(&initGPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // copyToGPUTime
            cudaMemcpy(deviceMatrixA, hostMatrixA, size * size * sizeof(float), cudaMemcpyHostToDevice);
            cudaMemcpy(deviceVectorB, hostVectorB, size * sizeof(float), cudaMemcpyHostToDevice);
        }
        Utils::Time::recorded(&copyToGPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // runGPUTime
            dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
            dim3 dimGrid((size + BLOCK_SIZE - 1) / BLOCK_SIZE, (size + BLOCK_SIZE - 1) / BLOCK_SIZE);
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    for (int i = 0; i < size; i++)
                        GPU_gauss_down<<<dimGrid, dimBlock>>>(deviceMatrixA, deviceVectorB, size, i);
                    for (int i = size - 1; i >= 0; i--)
                        GPU_gauss_up<<<dimGrid, dimBlock>>>(deviceMatrixA, deviceVectorB, size, i);
                    break;
                }
                case ModeMemory::SHARED: {
                    for (int i = 0; i < size; i++)
                        GPU_gauss_down_shared<<<dimGrid, dimBlock>>>(deviceMatrixA, deviceVectorB, size, i);
                    for (int i = size - 1; i >= 0; i--)
                        GPU_gauss_up_shared<<<dimGrid, dimBlock>>>(deviceMatrixA, deviceVectorB, size, i);
                    break;
                }
            }
        }
        Utils::Time::recorded(&runGPUTime, startCudaEvent, endCudaEvent);
        cudaEventRecord(startCudaEvent, nullptr);
        { // copyToCPUTime
            cudaMemcpy(hostMatrixA, deviceMatrixA, size * size * sizeof(float), cudaMemcpyDeviceToHost);
            cudaMemcpy(hostVectorB, deviceVectorB, size * sizeof(float), cudaMemcpyDeviceToHost);
        }
        Utils::Time::recorded(&copyToCPUTime, startCudaEvent, endCudaEvent);

    }
    Utils::Time::recorded(&allTime, allStartCudaEvent, allEndCudaEvent);

#ifdef ENABLE_DEBUG_MACRO
    Utils::Print::CPU_print_slay_system(hostMatrixA, hostVectorB, size);
    Utils::Print::CPU_print_result_vector(hostMatrixA, hostVectorB, hostVectorC, size);
#endif

    switch (timeMode) {
        case TimeMode::ALL: {
            std::cout << "TIME: init CPU: " << initCPUTime << " ms." << std::endl;
            std::cout << "TIME: fill: " << fillTime << " ms." << std::endl;
            std::cout << "TIME: copyToGPU: " << copyToGPUTime << " ms." << std::endl;
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    std::cout << "TIME: runGlobalGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
                case ModeMemory::SHARED: {
                    std::cout << "TIME: runSharedGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
            }
            std::cout << "TIME: copyToCPU: " << copyToCPUTime << " ms." << std::endl;
            std::cout << "TIME: all: " << allTime << " ms." << std::endl;
            break;
        }
        case TimeMode::FOR_EQUAL: {
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    std::cout << "TIME: runGlobalGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
                case ModeMemory::SHARED: {
                    std::cout << "TIME: runSharedGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
            }
            break;
        }
    }
    cudaEventDestroy(allStartCudaEvent);
    cudaEventDestroy(allEndCudaEvent);
    cudaEventDestroy(startCudaEvent);
    cudaEventDestroy(endCudaEvent);
    cudaFree(deviceMatrixA);
    cudaFree(deviceVectorB);
    free(hostMatrixA);
    free(hostVectorB);
    free(hostVectorC);
    cudaProfilerStop();
    cudaDeviceReset();
}

int
main(int argc, char *argv[]) {
    int size;
    try {
        size = std::stoi(argv[2]);
        if (std::string(argv[1]) == "shared") {
            kernel(ModeMemory::SHARED, size, TimeMode::ALL);
        } else if (std::string(argv[1]) == "global") {
            kernel(ModeMemory::GLOBAl, size, TimeMode::ALL);
        } else if (std::string(argv[1]) == "equals") {
            kernel(ModeMemory::GLOBAl, size, TimeMode::FOR_EQUAL);
            kernel(ModeMemory::SHARED, size, TimeMode::FOR_EQUAL);
        }
    } catch (const std::exception &exception) {
        exit(-1);
    }
    return 0;
}