#include <cuda.h>
#include "Utils.h"

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }


__global__ void
KernelJacobi(const float *deviceA, const float *deviceF, const float *deviceX0, float *deviceX1, int N) {
    float temp;
    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    deviceX1[x] = deviceF[x];
    for (int i = 0; i < N; i++) {
        if (x != i)
            deviceX1[x] -= deviceA[i + x * N] * deviceX0[i];
        else temp = deviceA[i + x * N];
    }
    deviceX1[x] /= temp;
}

__global__ void
KernelJacobiShared(const float *deviceA, const float *deviceF, const float *deviceX0, float *deviceX1, int N) {
    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned y = blockIdx.y;
    float matrixElement;
    float vectorElement;
    __shared__ float result;
    __shared__ float vector[BLOCK_SIZE];
    if (x < N) {
        matrixElement = deviceA[y * N + x];
        vectorElement = deviceX0[x];
        vector[x] = matrixElement * vectorElement;
    }
    if (x == 0) {
        result = -deviceF[y];
        for (int j = 0; j < N; j++)
            result += vector[j];
        result -= deviceA[y * N + y] * deviceX0[y];
        result /= -deviceA[y * N + y];
        deviceX1[y] = result;
    }
}

__global__ void
EpsJacobi(float *deviceX0, float *deviceX1, float *delta, int N) {
    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    delta[x] += fabs(deviceX0[x] - deviceX1[x]);
    deviceX0[x] = deviceX1[x];
}

void
kernel(ModeMemory const &mode, int const &size, TimeMode const timeMode) {

    float allTime = 0;
    float copyToCPUTime = 0;
    float runGPUTime = 0;
    float copyToGPUTime = 0;
    float initGPUTime = 0;
    float fillTime = 0;
    float initCPUTime = 0;

    cudaEvent_t allStartCudaEvent, allEndCudaEvent, startCudaEvent, endCudaEvent;

    cudaEventCreate(&allStartCudaEvent);
    cudaEventCreate(&allEndCudaEvent);
    cudaEventCreate(&startCudaEvent);
    cudaEventCreate(&endCudaEvent);

    float *hostA, *hostX, *hostX0, *hostX1, *hostF, *hostDelta;
    float *deviceA, *deviceX0, *deviceX1, *deviceF, *delta;

    float EPS = 1.e-5;

    int count = 0;
    float eps = 1.0f;

    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid((size + BLOCK_SIZE - 1) / BLOCK_SIZE, (size + BLOCK_SIZE - 1) / BLOCK_SIZE);

    unsigned mem_sizeA = sizeof(float) * size * size;
    unsigned mem_sizeX = sizeof(float) * size;

    cudaEventRecord(allStartCudaEvent, nullptr);
    { // allTime
        cudaEventRecord(startCudaEvent, nullptr);
        { // initCPUTime
            hostA = static_cast<float *>(malloc(mem_sizeA));
            hostF = static_cast<float *>(malloc(mem_sizeX));
            hostX = static_cast<float *>(malloc(mem_sizeX));
            hostX0 = static_cast<float *>(malloc(mem_sizeX));
            hostX1 = static_cast<float *>(malloc(mem_sizeX));
            hostDelta = static_cast<float *>(malloc(mem_sizeX));
        }
        Utils::Time::recorded(&initCPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // fillTime
            Utils::Fill::CPU_fill_matrix_rand(hostA, size, RAND_DISTRIBUTION_MIN, RAND_DISTRIBUTION_MAX);
            Utils::Fill::CPU_fill_vector_rand(hostX, size, RAND_DISTRIBUTION_MIN, RAND_DISTRIBUTION_MAX);
            Utils::Fill::CPU_fill_vector_rand(hostX1, size, 1, 1);
            Utils::Fill::CPU_fill_vector_rand(hostX0, size, 1, 1);
            Utils::Fill::CPU_fill_vector_rand(hostDelta, size, 0, 0);
            Utils::Fill::CPU_fill_vector_rand(hostDelta, size, 0, 0);
            float sum;
            for (int i = 0; i < size; i++) {
                sum = 0.0f;
                for (int j = 0; j < size; j++)
                    sum += hostA[j + i * size] * hostX[j];
                hostF[i] = sum;
            }
        }
        Utils::Time::recorded(&fillTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        {  // initGPUTime
            cudaMalloc(&deviceA, mem_sizeA);
            cudaMalloc(&deviceF, mem_sizeX);
            cudaMalloc(&deviceX0, mem_sizeX);
            cudaMalloc(&deviceX1, mem_sizeX);
            cudaMalloc(&delta, mem_sizeX);
        }
        Utils::Time::recorded(&initGPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        { // copyToGPUTime
            cudaMemcpy(deviceA, hostA, mem_sizeA, cudaMemcpyHostToDevice);
            cudaMemcpy(deviceF, hostF, mem_sizeX, cudaMemcpyHostToDevice);
            cudaMemcpy(deviceX0, hostX0, mem_sizeX, cudaMemcpyHostToDevice);
            cudaMemcpy(deviceX1, hostX1, mem_sizeX, cudaMemcpyHostToDevice);
        }
        Utils::Time::recorded(&copyToGPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        {  // runGPUTime
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    while (eps > EPS) {
                        count++;
                        CUDA_CHECK(cudaMemcpy(delta, hostDelta, mem_sizeX, cudaMemcpyHostToDevice));
                        KernelJacobi<<<dimGrid, dimBlock>>>(deviceA, deviceF, deviceX0, deviceX1, size);
                        EpsJacobi<<<dimGrid, dimBlock>>>(deviceX0, deviceX1, delta, size);
                        CUDA_CHECK(cudaMemcpy(hostDelta, delta, mem_sizeX, cudaMemcpyDeviceToHost));
                        eps = 0.0f;
                        for (int j = 0; j < size; j++) {
                            eps += hostDelta[j];
                            hostDelta[j] = 0;
                        }
                        eps = eps / static_cast<float>(size);
                    }
                    break;
                }
                case ModeMemory::SHARED: {
                    while (eps > EPS) {
                        count++;
                        CUDA_CHECK(cudaMemcpy(delta, hostDelta, mem_sizeX, cudaMemcpyHostToDevice));
                        KernelJacobiShared<<<dimGrid, dimBlock>>>(deviceA, deviceF, deviceX0, deviceX1, size);
                        EpsJacobi<<<dimGrid, dimBlock>>>(deviceX0, deviceX1, delta, size);
                        CUDA_CHECK(cudaMemcpy(hostDelta, delta, mem_sizeX, cudaMemcpyDeviceToHost));
                        eps = 0.0f;
                        for (int j = 0; j < size; j++) {
                            eps += hostDelta[j];
                            hostDelta[j] = 0;
                        }
                        eps = eps / static_cast<float>(size);
                    }
                    break;
                }
            }
        }
        Utils::Time::recorded(&runGPUTime, startCudaEvent, endCudaEvent);

        cudaEventRecord(startCudaEvent, nullptr);
        {  // copyToCPUTime
            cudaMemcpy(hostX1, deviceX1, mem_sizeX, cudaMemcpyDeviceToHost);
        }
        Utils::Time::recorded(&copyToCPUTime, startCudaEvent, endCudaEvent);

    }
    Utils::Time::recorded(&allTime, allStartCudaEvent, allEndCudaEvent);
    switch (timeMode) {
        case TimeMode::ALL: {
            std::cout << "TIME: init CPU: " << initCPUTime << " ms." << std::endl;
            std::cout << "TIME: fill: " << fillTime << " ms." << std::endl;
            std::cout << "TIME: copyToGPU: " << copyToGPUTime << " ms." << std::endl;
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    std::cout << "TIME: runGlobalGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
                case ModeMemory::SHARED: {
                    std::cout << "TIME: runSharedGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
            }
            std::cout << "TIME: copyToCPU: " << copyToCPUTime << " ms." << std::endl;
            std::cout << "TIME: all: " << allTime << " ms." << std::endl;
            break;
        }
        case TimeMode::FOR_EQUAL: {
            switch (mode) {
                case ModeMemory::GLOBAl: {
                    std::cout << "TIME: runGlobalGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
                case ModeMemory::SHARED: {
                    std::cout << "TIME: runSharedGPU: " << runGPUTime << " ms." << std::endl;
                    break;
                }
            }
            break;
        }
    }
    free(hostA);
    free(hostF);
    free(hostX0);
    free(hostX1);
    free(hostX);
    free(hostDelta);
    cudaFree(deviceA);
    cudaFree(deviceF);
    cudaFree(deviceX0);
    cudaFree(deviceX1);
    cudaEventDestroy(allStartCudaEvent);
    cudaEventDestroy(allEndCudaEvent);
    cudaEventDestroy(startCudaEvent);
    cudaEventDestroy(endCudaEvent);
    cudaDeviceReset();
}

int
main(int argc, char *argv[]) {
    int size;
    try {
        size = std::stoi(argv[2]);
        if (std::string(argv[1]) == "shared") {
            kernel(ModeMemory::SHARED, size, TimeMode::ALL);
        } else if (std::string(argv[1]) == "global") {
            kernel(ModeMemory::GLOBAl, size, TimeMode::ALL);
        } else if (std::string(argv[1]) == "equals") {
            kernel(ModeMemory::GLOBAl, size, TimeMode::FOR_EQUAL);
            kernel(ModeMemory::SHARED, size, TimeMode::FOR_EQUAL);
        }
    } catch (std::exception &exception) {
        exit(-1);
    }
    return 0;
}