#pragma once

#include <cstdlib>
#include <iostream>
#include <random>
#include <driver_types.h>

#define RAND_DISTRIBUTION_MIN -100
#define RAND_DISTRIBUTION_MAX 100

#define BLOCK_SIZE 64

enum class ModeMemory {
    SHARED,
    GLOBAl
};
enum class TimeMode {
    ALL,
    FOR_EQUAL
};


namespace Utils {
    namespace Print {
        inline void
        CPU_print_slay_system(const float *matrix_X, const float *vector_X, const int &size) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++)
                    std::cout << matrix_X[j + i * size] << "\t";
                std::cout << "| = " << vector_X[i] << std::endl;
            }
            std::cout << std::endl;
        }

        inline void
        CPU_print_matrix(const float *matrix, const int &size) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++)
                    std::cout << matrix[j + i * size] << "\t";
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }

        inline void
        CPU_print_vector(const float *vector, const int &size) {
            for (int i = 0; i < size; i++)
                std::cout << vector[i] << "\t";
            std::cout << std::endl;
        }

        inline void
        CPU_print_result_vector(const float *matrixA, const float *vectorB, float *vectorC, int size) {
            for (int i = 0; i < size - 1; i++) {
                vectorC[i] = vectorB[i] / matrixA[i * size + i];
                std::cout << vectorC[i] << "\t";
            }
            std::cout << std::endl << vectorC[size - 1] << std::endl;
        }
    }
    namespace Fill {
        inline void
        CPU_fill_matrix_rand(float *matrix, const int size, const int distributionStart, const int distributionEnd) {
            std::random_device randomDevice{};
            std::mt19937 engine{randomDevice()};
            std::uniform_real_distribution<float> distribution(distributionStart, distributionEnd);
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    matrix[i * size + j] = distribution(engine);
        }

        inline void
        CPU_fill_vector_rand(float *vector, const int size, const int distributionStart, const int distributionEnd) {
            std::random_device randomDevice{};
            std::mt19937 engine{randomDevice()};
            std::uniform_real_distribution<float> distribution(distributionStart, distributionEnd);
            for (int i = 0; i < size; i++)
                vector[i] = distribution(engine);
        }

        inline void
        CPU_fill_matrix_static(float *matrix, const int size) {
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    matrix[i * size + j] = static_cast<float>(j + i);
        }

        inline void
        CPU_fill_vector_static(float *vector, const int size) {
            for (int i = 0; i < size; i++)
                vector[i] = static_cast<float>(i);
        }
    }
    namespace Time {
        inline void
        recorded(float *time, cudaEvent_t &start, cudaEvent_t &end) {
            cudaEventRecord(end, nullptr);
            cudaEventSynchronize(end);
            cudaEventElapsedTime(time, start, end);
        }
    }
}