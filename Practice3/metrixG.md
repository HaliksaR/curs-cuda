## P3G global 3000
```shell script
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> nvprof ./P3G global 3000==8993== NVPROF is profiling process 8993, command: ./P3G global 3000
TIME: init CPU: 0.009184 ms.
TIME: fill: 1032.65 ms.
TIME: copyToGPU: 5.09536 ms.
TIME: runGlobalGPU: 7.57178 ms.
TIME: copyToCPU: 4.23776 ms.
TIME: all: 1049.95 ms.
==8993== Profiling application: ./P3G global 3000
==8993== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   54.59%  4.9479ms         2  2.4740ms  1.8240us  4.9461ms  [CUDA memcpy HtoD]
                   45.41%  4.1166ms         2  2.0583ms  1.8560us  4.1147ms  [CUDA memcpy DtoH]
      API calls:   61.20%  126.17ms         4  31.543ms     438ns  126.17ms  cudaEventCreate
                   30.69%  63.262ms         1  63.262ms  63.262ms  63.262ms  cudaDeviceReset
                    4.53%  9.3291ms         4  2.3323ms  13.615us  5.0113ms  cudaMemcpy
                    1.23%  2.5257ms     24000     105ns      85ns  223.52us  cudaSetupArgument
                    0.71%  1.4723ms         2  736.15us  169.39us  1.3029ms  cudaFree
                    0.56%  1.1623ms      6000     193ns     178ns     791ns  cudaLaunch
                    0.46%  939.65us       188  4.9980us     103ns  233.32us  cuDeviceGetAttribute
                    0.27%  561.26us      6000      93ns      85ns  2.6860us  cudaConfigureCall
                    0.17%  341.67us         2  170.84us  120.58us  221.10us  cudaMalloc
                    0.06%  129.98us         7  18.568us  2.9270us  65.054us  cudaEventSynchronize
                    0.05%  107.63us         2  53.816us  49.441us  58.192us  cuDeviceTotalMem
                    0.05%  95.205us         2  47.602us  47.320us  47.885us  cuDeviceGetName
                    0.02%  37.998us        14  2.7140us  1.0310us  12.038us  cudaEventRecord
                    0.00%  8.7890us         7  1.2550us  1.0540us  1.7060us  cudaEventElapsedTime
                    0.00%  3.2670us         4     816ns     428ns  1.8490us  cudaEventDestroy
                    0.00%  1.4670us         3     489ns     118ns  1.2120us  cuDeviceGetCount
                    0.00%     833ns         4     208ns     113ns     474ns  cuDeviceGet
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> 
```

## P3G shared 3000
```shell script
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> nvprof ./P3G shared 3000
==9094== NVPROF is profiling process 9094, command: ./P3G shared 3000
TIME: init CPU: 0.0088 ms.
TIME: fill: 1033.27 ms.
TIME: copyToGPU: 3.71594 ms.
TIME: runSharedGPU: 7.22874 ms.
TIME: copyToCPU: 4.17325 ms.
TIME: all: 1048.78 ms.
==9094== Profiling application: ./P3G shared 3000
==9094== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   52.93%  4.0465ms         2  2.0233ms  1.8240us  4.0447ms  [CUDA memcpy DtoH]
                   47.07%  3.5981ms         2  1.7991ms  1.8240us  3.5963ms  [CUDA memcpy HtoD]
      API calls:   61.75%  126.80ms         4  31.699ms     485ns  126.80ms  cudaEventCreate
                   30.89%  63.430ms         1  63.430ms  63.430ms  63.430ms  cudaDeviceReset
                    3.84%  7.8845ms         4  1.9711ms  13.488us  4.1584ms  cudaMemcpy
                    1.20%  2.4711ms     24000     102ns      84ns  224.17us  cudaSetupArgument
                    0.71%  1.4595ms         2  729.75us  160.32us  1.2992ms  cudaFree
                    0.53%  1.0891ms      6000     181ns     174ns  1.8350us  cudaLaunch
                    0.47%  973.30us       188  5.1770us     104ns  239.77us  cuDeviceGetAttribute
                    0.26%  543.70us      6000      90ns      84ns  3.3030us  cudaConfigureCall
                    0.17%  342.13us         2  171.06us  121.80us  220.32us  cudaMalloc
                    0.06%  122.34us         2  61.171us  56.788us  65.554us  cuDeviceTotalMem
                    0.05%  98.566us         2  49.283us  49.026us  49.540us  cuDeviceGetName
                    0.03%  68.414us         7  9.7730us  2.9430us  46.628us  cudaEventSynchronize
                    0.02%  34.923us        14  2.4940us     987ns  11.921us  cudaEventRecord
                    0.00%  8.0740us         7  1.1530us     956ns  1.5040us  cudaEventElapsedTime
                    0.00%  3.3840us         4     846ns     418ns  1.9820us  cudaEventDestroy
                    0.00%  1.5380us         3     512ns     108ns  1.1570us  cuDeviceGetCount
                    0.00%  1.2250us         4     306ns     122ns     489ns  cuDeviceGet
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> 
```

