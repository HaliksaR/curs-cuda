## P3I global 3000
```shell script
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> nvprof ./P3I global 3000                         ==23630== NVPROF is profiling process 23630, command: ./P3I global 3000
TIME: init CPU: 0.010496 ms.
TIME: fill: 1056.51 ms.
TIME: copyToGPU: 4.92445 ms.
TIME: runGlobalGPU: 0.036864 ms.
TIME: copyToCPU: 0.012032 ms.
TIME: all: 1061.94 ms.
==23630== Profiling application: ./P3I global 3000
==23630== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   99.93%  4.7908ms         5  958.15us  1.7920us  4.7835ms  [CUDA memcpy HtoD]
                    0.07%  3.4240us         2  1.7120us  1.6960us  1.7280us  [CUDA memcpy DtoH]
      API calls:   64.10%  126.14ms         4  31.536ms     453ns  126.14ms  cudaEventCreate
                   32.25%  63.459ms         1  63.459ms  63.459ms  63.459ms  cudaDeviceReset
                    2.52%  4.9512ms         7  707.31us  6.2350us  4.8222ms  cudaMemcpy
                    0.66%  1.3025ms       188  6.9280us     163ns  367.32us  cuDeviceGetAttribute
                    0.20%  400.92us         5  80.183us  4.6600us  254.18us  cudaMalloc
                    0.08%  156.07us         4  39.016us  4.5250us  138.76us  cudaFree
                    0.07%  137.90us         2  68.948us  65.054us  72.842us  cuDeviceTotalMem
                    0.06%  118.94us         2  59.468us  55.834us  63.103us  cuDeviceGetName
                    0.03%  68.410us         7  9.7720us  2.7260us  46.746us  cudaEventSynchronize
                    0.02%  31.397us        14  2.2420us     986ns  11.506us  cudaEventRecord
                    0.00%  8.1640us         7  1.1660us     873ns  1.7500us  cudaEventElapsedTime
                    0.00%  4.5750us         9     508ns      88ns  3.4940us  cudaSetupArgument
                    0.00%  2.6480us         4     662ns     378ns  1.4380us  cudaEventDestroy
                    0.00%  1.9820us         3     660ns     195ns  1.5530us  cuDeviceGetCount
                    0.00%  1.4460us         4     361ns     163ns     916ns  cuDeviceGet
                    0.00%  1.0060us         2     503ns     185ns     821ns  cudaLaunch
                    0.00%     861ns         2     430ns     143ns     718ns  cudaConfigureCall
```

## P3I shared 3000
```shell script
is-742@linux-47dw:~/valok/curs-cuda/cmake-build-debug> nvprof ./P3I shared 3000
==23652== NVPROF is profiling process 23652, command: ./P3I shared 3000
TIME: init CPU: 0.010912 ms.
TIME: fill: 1055.88 ms.
TIME: copyToGPU: 5.2385 ms.
TIME: runSharedGPU: 0.036512 ms.
TIME: copyToCPU: 0.011808 ms.
TIME: all: 1061.58 ms.
==23652== Profiling application: ./P3I shared 3000
==23652== Profiling result:
            Type  Time(%)      Time     Calls       Avg       Min       Max  Name
 GPU activities:   99.93%  5.0978ms         5  1.0196ms  1.7920us  5.0905ms  [CUDA memcpy HtoD]
                    0.07%  3.5200us         2  1.7600us  1.7280us  1.7920us  [CUDA memcpy DtoH]
      API calls:   64.36%  127.53ms         4  31.883ms     469ns  127.53ms  cudaEventCreate
                   32.03%  63.475ms         1  63.475ms  63.475ms  63.475ms  cudaDeviceReset
                    2.66%  5.2645ms         7  752.08us  6.1090us  5.1382ms  cudaMemcpy
                    0.52%  1.0300ms       188  5.4780us     102ns  272.40us  cuDeviceGetAttribute
                    0.18%  354.72us         5  70.944us  4.4760us  215.98us  cudaMalloc
                    0.09%  171.49us         4  42.872us  4.3420us  153.82us  cudaFree
                    0.05%  108.19us         2  54.096us  49.416us  58.777us  cuDeviceTotalMem
                    0.05%  94.701us         2  47.350us  46.407us  48.294us  cuDeviceGetName
                    0.03%  67.842us         7  9.6910us  2.8100us  46.118us  cudaEventSynchronize
                    0.02%  31.683us        14  2.2630us  1.0300us  12.128us  cudaEventRecord
                    0.00%  8.2060us         7  1.1720us     889ns  1.6660us  cudaEventElapsedTime
                    0.00%  4.7520us         9     528ns      91ns  3.7100us  cudaSetupArgument
                    0.00%  2.6120us         4     653ns     377ns  1.4180us  cudaEventDestroy
                    0.00%  1.4700us         3     490ns     131ns  1.1760us  cuDeviceGetCount
                    0.00%     957ns         2     478ns     193ns     764ns  cudaLaunch
                    0.00%     877ns         4     219ns     100ns     524ns  cuDeviceGet
                    0.00%     796ns         2     398ns     123ns     673ns  cudaConfigureCall
```

