Контрольные вопросы:

## 1. Архитектура GPU.
- Разделяют два вида устройств – то которое управляет общей логикой – host, и то которое умеет быстро выполнить некоторый набор инструкций над большим объемом данных – device.
- ![](https://my-it-notes.com/wp-content/uploads/2013/06/Architecture.png)
- В роли хоста обычно выступает центральный процессор (CPU – например i5/i7).
  В роли вычислительного устройства – видеокарта (GPU – GTX690/HD7970). Видеокарта содержит Compute Units – процессорные ядра. Неразбериху вводят и производители NVidia называет свои Streaming Multiprocessor unit или SMX , а
  ATI – SIMD Engine или Vector Processor. В современных игровых видеокартах – их 8-32.
  Процессорные ядра могут исполнять несколько потоков за счет того, что в каждом содержится несколько (8-16) потоковых процессоров (Stream Cores или Stream Processor). Для карт NVidia – вычисления производятся непосредственно на потоковых процессорах, но ATI ввели еще один уровень абстракции – каждый потоковый процессор, состоит из processing elements – PE (иногда называемых ALU – arithmetic and logic unit) – и вычисления происходят на них.
> http://my-it-notes.com/2013/06/gpu-processing-intro-cuda-opencl/
>
> http://ssd.sscc.ru/sites/default/files/content/attach/332/cuda-3-arch.pdf

## 2. Отличие архитектуры GPU от архитектуры CPU
- CPU отличается от GPU в первую очередь способами доступа к памяти. В GPU он связанный и легко предсказуемый — если из памяти читается тексел текстуры, то через некоторое время настанет очередь и соседних текселов. С записью похожая ситуация — пиксель записывается во фреймбуфер, и через несколько тактов будет записываться расположенный рядом с ним.  Также графическому процессору, в отличие от универсальных процессоров, просто не нужна кэш-память большого размера, а для текстур требуются лишь 128–256 килобайт. Кроме того, на видеокартах применяется более быстрая память, и в результате GPU доступна в разы большая пропускная способность, что также весьма важно для параллельных расчетов, оперирующих с огромными потоками данных.
- Есть множество различий и в поддержке многопоточности: CPU исполняет 1–2 потока вычислений на одно процессорное ядро, а GPU может поддерживать несколько тысяч потоков на каждый мультипроцессор, которых в чипе несколько штук! И если переключение с одного потока на другой для CPU стоит сотни тактов, то GPU переключает несколько потоков за один такт.
- В CPU большая часть площади чипа занята под буферы команд, аппаратное предсказание ветвления и огромные объемы кэш-памяти, а в GPU большая часть площади занята исполнительными блоками. Вышеописанное устройство схематично изображено ниже:
- ![](https://s3.tproger.ru/uploads/2015/11/compare_paper.jpg)

## 3. Программная модель CUDA
- CUDA-Compute Unified Device Architecture
- Программная модель – включает вычислительный параллелизм и структуру памяти непосредственно в язык программирования
- Основные принципы
    - Программана CUDA использует как CPU так и GPU 
    - Программа состоит из последовательныхи параллельных участков кода 
    - На CPU, который называют host, выполняется последовательная часть кода, подготовка и вызов GPU кода
    - На GPU, который называют device, выполняется параллельнаячасть кода 
    - CPU и GPU обладают независимой раздельной памятью
    - Код на GPU называется ядром(kernel)
    - Ядро выполняется параллельно множеством нитей/потоков(threads)
    - Каждый поток выполняет один и тот же код
    - Разница между нитями CPU и GPU:
        - Нить GPU очень легкая, контекст минимален, выделение происходит быстро
        - Эффективное использование GPU – вызов тысячи нитей
        - Максимальная эффективность на CPU – количество нитей равно числу ядер, или кратно больше
> https://docplayer.ru/37092414-Programmnaya-model-cuda.html

## 4. Иерархия памяти GPU
- файл регистров
- разделяемая память
- кэш текстурный
- кэш константный
- кэш L1
- кэш L2
- глобальная память

## 5. Глобальная память GPU. Шаблон использования, оптимизация доступа, метрики производительности работы с глобальной памятью (в т.ч. метрики профилировщика)
- Глобальная память (global memory) – самый медленный тип памяти, из доступных GPU. Глобальные переменные можно выделить с помощью спецификатора __global__, а так же динамически, с помощью функций из семейства cudMallocXXX. Глобальная память в основном служит для хранения больших объемов данных, поступивших на device с host’а, данное перемещение осуществляется с использованием функций cudaMemcpyXXX. В алгоритмах, требующих высокой производительности, количество операций с глобальной памятью необходимо свести к минимуму
- profiler metrics
    - global_access
    - atomic_throughput
    - atomic_transactions
    - atomic_transactions_per_request
    - gld_efficiency
    - gld_requested_throughput
    - gld_throughput 
    - gld_transactions
    - gld_transactions_per_request
    - global_cache_replay_overhead
    - global_replay_overhead
    - gst_efficiency
    - gst_requested_throughput
    - gst_throughput
    - gst_transactions
    - gst_transactions_per_request
    - ldst_executed
    - ldst_fu_utilization
    - ldst_issued
    - nc_gld_efficiency
    - nc_gld_requested_throughput
    - nc_gld_throughput
    - global_atomic_requests
    - global_hit_rate
    - global_load_requests
    - global_reduction_requests
    - global_store_requests

## 6. Разделяемая память GPU. Шаблон использования, оптимизация доступа, метрики производительности работы с разделяемой памятью (в т.ч. метрики профилировщика)
- Разделяемая память (shared memory) относиться к быстрому типу памяти. Разделяемую память рекомендуется использовать для минимизации обращение к глобальной памяти, а так же для хранения локальных переменных функций. Адресация разделяемой памяти между нитями потока одинакова в пределах одного блока, что может быть использовано для обмена данными между потоками в пределах одного блока. Для размещения данных в разделяемой памяти используется спецификатор __shared__.
- profiler metrics
    - shared_access
    - l1_shared_utilization
    - ldst_executed
    - ldst_fu_utilization
    - ldst_issued
    - shared_efficiency
    - shared_load_throughput
    - shared_load_transactions
    - shared_load_transactions_per_request
    - shared_replay_overhead
    - shared_store_throughput
    - shared_store_transactions
    - shared_store_transactions_per_request
    - shared_utilization
    
    
## 7. Теоретическая и достигнутая заполняемость. Ограничивающие заполняемость факторы.
- Occupancy = Active Warps / Maximum Active Warps
> https://stackoverflow.com/a/50176880
- Remember: resources are allocated for the entire block
    - Resources are finite
    - Utilizing too many resources per thread may limit the occupancy
- Potential occupancy limiters:
    - Register usage
    - Shared memory usage
    - Block size
> http://on-demand.gputechconf.com/gtc-express/2011/presentations/cuda_webinars_WarpsAndOccupancy.pdf
- Как блоки распределяются в SM в CUDA, когда их количество меньше доступных SM?
> https://ru.programqa.com/question/43632483/

> https://ru.programqa.com/question/23469180/different-occupancy-between-calculator-and-nvprof

> https://docs.nvidia.com/gameworks/content/developertools/desktop/analysis/report/cudaexperiments/kernellevel/achievedoccupancy.htm