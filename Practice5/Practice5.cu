#include <cstdio>
#include <cmath>
#include <cuda.h>
#include <iostream>

#define I_MIN(A, B) (A<B?A:B)

const float RADIUS = 10.0f;
const int K = 48;
const int VERT_COUNT = K * K * 2;
const int THREADS_PER_BLOCK = 256;
const int BLOCKS_PER_GRID = I_MIN(32, (VERT_COUNT + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK);
const int SIZE = 32;
const int FIT = SIZE / 2;

struct Vertex {
    float x;
    float y;
    float z;
};

__constant__ Vertex vert_const[VERT_COUNT];
Vertex vert[VERT_COUNT];

float func_CPU(float x, float y, float z);

__device__ float func_GPU(float x, float y, float z);

double check(const Vertex *pVertex);

void CreateVertexes();

void calculate(float *array3D);

void createTexture(float *array3D, cudaArray *pArray, cudaTextureObject_t *texture);

__global__ void kernelTexture(float *sum, cudaTextureObject_t texture);

__global__ void kernelBase(float *sum);

__device__ float interpolation();

__device__ bool bit(unsigned int i, unsigned int position);

int main(int argc, char **argv) {
    CreateVertexes();

    auto *array3D = new float[SIZE * SIZE * SIZE];
    calculate(array3D);

    cudaArray *dstArray = nullptr;
    cudaTextureObject_t texture;
    createTexture(array3D, dstArray, &texture);

    auto *sumHost = new float[BLOCKS_PER_GRID];
    float *sumDevice;
    cudaMalloc((void **) &sumDevice, sizeof(float) * BLOCKS_PER_GRID);
    cudaEvent_t start, stop;


    { //Интерполяция сферы с использованием текстурной и константной памяти
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, nullptr);

        kernelTexture <<< BLOCKS_PER_GRID, THREADS_PER_BLOCK >>>(sumDevice, texture);
        cudaDeviceSynchronize();

        cudaMemcpy(sumHost, sumDevice, sizeof(float) * BLOCKS_PER_GRID, cudaMemcpyDeviceToHost);

        float s = 0.0f;
        for (int i = 0; i < BLOCKS_PER_GRID; i++)
            s += sumHost[i];
        printf("sum Texture = %f\n", s * M_PI * M_PI / K / K);

        cudaEventRecord(stop, nullptr);
        cudaEventSynchronize(stop);
        float elapsedTime;
        cudaEventElapsedTime(&elapsedTime, start, stop);
        std::cout << "TIME: " << elapsedTime << " ms." << std::endl;
    }


    { //Интерполяция сферы без использованием текстурной и константной памяти
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        cudaEventRecord(start, nullptr);

        kernelBase <<<BLOCKS_PER_GRID, THREADS_PER_BLOCK>>>(sumDevice);
        cudaDeviceSynchronize();

        cudaMemcpy(sumHost, sumDevice, sizeof(float) * BLOCKS_PER_GRID, cudaMemcpyDeviceToHost);

        float s = 0.0f;
        for (int i = 0; i < BLOCKS_PER_GRID; i++)
            s += sumHost[i];
        printf("sum Base = %f\n", s * M_PI * M_PI / K / K);

        cudaEventRecord(stop, nullptr);
        cudaEventSynchronize(stop);
        float elapsedTime;
        cudaEventElapsedTime(&elapsedTime, start, stop);
        std::cout << "TIME: " << elapsedTime << " ms." << std::endl;
    }
    cudaEventDestroy(start);
    cudaEventDestroy(stop);


    cudaFree(sumDevice);
    cudaFreeArray(dstArray);
    cudaDestroyTextureObject(texture);
    delete[] sumHost;
    delete[] array3D;
    return 0;
}

__global__ void kernelTexture(float *sum, cudaTextureObject_t texture) {
    __shared__ float shared[THREADS_PER_BLOCK];
    unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned int sharedIndex = threadIdx.x;

    shared[sharedIndex] = tex3D<float>(
            texture,
            static_cast<float>(vert_const[tid].x + FIT + 0.5f),
            static_cast<float>(vert_const[tid].y + FIT + 0.5f),
            static_cast<float>(vert_const[tid].z + FIT + 0.5f)
    );
    __syncthreads();

    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1u) {
        if (sharedIndex < s) shared[sharedIndex] += shared[sharedIndex + s];
        __syncthreads();
    }

    if (sharedIndex == 0)
        sum[blockIdx.x] = shared[0];
}

__global__ void kernelBase(float *sum) {
    __shared__ float shared[THREADS_PER_BLOCK];

    unsigned int cacheIndex = threadIdx.x;

    shared[cacheIndex] = interpolation();

    __syncthreads();
    for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1u) {
        if (cacheIndex < s)
            shared[cacheIndex] += shared[cacheIndex + s];
        __syncthreads();
    }

    if (cacheIndex == 0) sum[blockIdx.x] = shared[0];
}

__device__ float interpolation() {
    unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;
    Vertex interPoint[2];
    Vertex mainPoint{}; //Искомая точка

    //Получаем координатные значения для точек
    for (int i = 0; i < 2; i++) {
        unsigned int i_phi = (tid + i) / K;
        unsigned int i_psi = (tid + i) % K;
        auto phi = static_cast<float>(i_phi * M_PI / K);
        auto psi = static_cast<float>(i_psi * M_PI / K);
        interPoint[i] = {
                RADIUS * sinf(psi) * cosf(phi),
                RADIUS * sinf(psi) * sinf(phi),
                RADIUS * cosf(psi)
        };
    }

    //Получение координат точки в середине куба
    mainPoint = {
            (interPoint[0].x + interPoint[1].x) / 2.0f,
            (interPoint[0].y + interPoint[1].y) / 2.0f,
            (interPoint[0].z + interPoint[1].z) / 2.0f
    };

    float k = (interPoint[1].x - interPoint[0].x) *
              (interPoint[1].y - interPoint[0].y) *
              (interPoint[1].z - interPoint[0].z);

    float tempSum = 0;

    if (k == 0.0f) return 0.0f;
    for (int i = 0; i < 8; i++) {
        float funcValue = func_GPU(
                interPoint[(int) bit(i, 0)].x,
                interPoint[(int) bit(i, 1)].y,
                interPoint[(int) bit(i, 2)].z
        );
        funcValue *=
                (bit(i, 0) ? mainPoint.x - interPoint[0].x : interPoint[1].x - mainPoint.x) *
                (bit(i, 1) ? mainPoint.y - interPoint[0].y : interPoint[1].y - mainPoint.y) *
                (bit(i, 2) ? mainPoint.z - interPoint[0].z : interPoint[1].z - mainPoint.z);

        tempSum += funcValue;
    }

    return tempSum / k;
}

__device__ bool bit(const unsigned int i, const unsigned int position) { return (bool(1u << position) & i); }

void createTexture(float *array3D, cudaArray *pArray, cudaTextureObject_t *texture) {
//cudaArray Descriptor
    const cudaExtent volumeSize = make_cudaExtent(
            SIZE * sizeof(float),
            SIZE,
            SIZE
    );
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
//cuda Array
    cudaMalloc3DArray(&pArray, &channelDesc, volumeSize);
    cudaMemcpy3DParms copyParams = {nullptr};
//Array creation
    copyParams.srcPtr = make_cudaPitchedPtr(
            array3D,
            SIZE * sizeof(float),
            SIZE,
            SIZE
    );
    copyParams.dstArray = pArray;
    copyParams.extent = make_cudaExtent(SIZE, SIZE, SIZE);
    copyParams.kind = cudaMemcpyDeviceToDevice;
    cudaMemcpy3D(&copyParams);
//Array creation End
    cudaResourceDesc texRes{};
    memset(&texRes, 0, sizeof(cudaResourceDesc));
    texRes.resType = cudaResourceTypeArray;
    texRes.res.array.array = pArray;

    cudaTextureDesc texDesc{};
    memset(&texDesc, 0, sizeof(cudaTextureDesc));
    texDesc.normalizedCoords = false;
    texDesc.filterMode = cudaFilterModeLinear;
    texDesc.addressMode[0] = cudaAddressModeClamp;
    texDesc.addressMode[1] = cudaAddressModeClamp;
    texDesc.addressMode[2] = cudaAddressModeClamp;
    texDesc.readMode = cudaReadModeElementType;

    cudaCreateTextureObject(texture, &texRes, &texDesc, nullptr);
}

void calculate(float *array3D) {
    for (int x = 0; x < SIZE; ++x)
        for (int y = 0; y < SIZE; ++y)
            for (int z = 0; z < SIZE; ++z)
                array3D[SIZE * (x * SIZE + y) + z] = func_CPU(
                        static_cast<float>(x - FIT),
                        static_cast<float>(y - FIT),
                        static_cast<float>(z - FIT)
                );
}

void CreateVertexes() {
    auto *temp_vert = new Vertex[VERT_COUNT];
    for (int i_phi = 0, i = 0; i_phi < 2 * K; i_phi++) {
        for (int i_psi = 0; i_psi < K; i_psi++, i++) {
            auto phi = static_cast<float>(i_phi * M_PI / K);
            auto psi = static_cast<float>(i_psi * M_PI / K);
            temp_vert[i] = {
                    RADIUS * sinf(psi) * cosf(phi),
                    RADIUS * sinf(psi) * sinf(phi),
                    RADIUS * cosf(psi)
            };
        }
    }
    std::cout << "sum check = " << check(temp_vert) * M_PI * M_PI / K / K << std::endl;

    cudaMemcpyToSymbol(vert_const, temp_vert, sizeof(Vertex) * VERT_COUNT, 0, cudaMemcpyHostToDevice);
    cudaMemcpy(vert, temp_vert, sizeof(Vertex) * VERT_COUNT, cudaMemcpyHostToDevice);

    delete[] temp_vert;
}

double check(const Vertex *pVertex) {
    float sum = 0.0f;
    for (int i = 0; i < VERT_COUNT; i++)
        sum += func_CPU(pVertex[i].x, pVertex[i].y, pVertex[i].z);
    return sum;
}

float func_CPU(const float x, const float y, const float z) {
    return static_cast<float>(
            pow((0.5 * sqrtf(15.0 / M_PI)), 2) *
            pow(z, 2) *
            pow(y, 2) *
            sqrtf(static_cast<float>((1.0f - z * z / pow(RADIUS, 2)))) /
            pow(RADIUS, 4)
    );
}

__device__ float func_GPU(const float x, const float y, const float z) {
    return static_cast<float>(
            pow((0.5 * sqrtf(15.0 / M_PI)), 2) *
            pow(z, 2) *
            pow(y, 2) *
            sqrtf(static_cast<float>((1.0f - z * z / pow(RADIUS, 2)))) /
            sqrtf(static_cast<float>((1.0f - z * z / pow(RADIUS, 2)))) /
            pow(RADIUS, 4)
    );
}
