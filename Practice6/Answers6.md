Контрольные вопросы: https://github.com/mlkv52git/sibsutis_cuda_bachelors/blob/master/Lectures/Lecture6.pdf

## 1. Какие операции, выполняемые хостом, не позволяют выполняться двум командам из разных потоков параллельно?
- 


## 2. Что такое portable memory?
- Портативная память: установите флаг cudaH 

## 4. Что такое mapped memory?
- mapped memory позволяет ядру напрямую обращаться к памяти хоста. Передача данных осуществляется неявно по мере обращения ядра. Таким образом, передача данных также может осуществляться параллельно.
> https://jhui.github.io/2017/03/06/CUDA/

## 5. Что такое CUDA поток?
- Поток - это последовательность команд, которые выполняются последовательно. Разные потоки, могут выполнять свои команды одновременно. Ядра, запущенные в одном и том же потоке, гарантированно выполняются последовательно, в то время как ядра, запущенные в разных потоках, могут выполняться одновременно.
> https://jhui.github.io/2017/03/06/CUDA/


## 6. Что такое page-locked memory?
- Вместо того чтобы использовать malloc при выделении памяти хоста, мы вызываем CUDA для создания page-locked memory хоста.
- page-locked memory имеет несколько преимуществ:
  + Асинхронное параллельное выполнение ядер и передача памяти с использованием потоковой передачи.
  + С mapped memory это устраняет необходимость выделять блок в памяти устройства.
  + Более высокая пропускная способность между памятью хоста и устройства, в частности, с write-combining memory.
- page-locked memory управляется с помощью:
  + cudaHostAlloc и cudaFreeHost выделение и освобождение page-locked memory.
  + страница cudaHostRegister-блокирует память, выделенную malloc
> https://jhui.github.io/2017/03/06/CUDA/

## 7. Что делает функция cudaHostAlloc
- ```C++
  __host__​cudaError_t cudaHostAlloc ( void** pHost, size_t size, unsigned int  flags )
  ```
- Выделяет заблокированную страницу памяти на хосте.
> https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html


## 8. Что делает функция cudaMemcpyAsync?
- ```C++
  __host__​cudaError_t cudaMemcpyAsync(void * dst, const void * src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream)
  ```
- Копирует количество байтов из области памяти, на которую указывает src, в область памяти, на которую указывает dst, где kind - это один из cudaMemcpyHostToHost, cudaMemcpyHostToDevice, cudaMemcpyDeviceToHost или cudaMemcpyDeviceToDevice, и указывает направление копирования. Области памяти могут не перекрываться. Вызов функции cudaMemcpyAsync() с помощью dst и указатели src, которые не соответствуют направлению копирования, приводят к неопределенному поведению.
>  http://developer.download.nvidia.com/compute/cuda/2_3/toolkit/docs/online/group__CUDART__MEMORY_ge4366f68c6fa8c85141448f187d2aa13.html


## 9. Как запустить функцию-ядро в определённом потоке?
- ```C++
  for (int i = 0; i < 3; ++i) {
      cudaMemcpyAsync(inputDevPtr + i * size, hostPtr + i * size, size, cudaMemcpyHostToDevice, stream[i]);
      MyKernel<<<100, 512, 0, stream[i]>>>(outputDevPtr + i * size, inputDevPtr + i * size, size);
      cudaMemcpyAsync(hostPtr + i * size, outputDevPtr + i * size,size, cudaMemcpyDeviceToHost, stream[i]);
  }
  ```
> https://jhui.github.io/2017/03/06/CUDA/


## 10. Как можно встроить в поток вызов функции хоста?
- Вроде бы использование колбеков? Ну по крайней мере в ресурсах которые я встречал поверхностно об этом говорили. Ну и в принципе звучит весьма убедительно, при том что я использую данный подход часто в котлин корутинах.
> - https://books.google.ru/books?id=dhWzDwAAQBAJ&pg=PA193&lpg=PA193&dq=cuda++host+function+call+in+stream&source=bl&ots=CrVVtg072b&sig=ACfU3U0iM8Lgk9vCxH3lRiQJel5zSnWXSg&hl=en&sa=X&ved=2ahUKEwiRhZ_wh4PpAhUUBBAIHZakAQ8Q6AEwB3oECAkQAQ#v=onepage&q=cuda%20%20host%20function%20call%20in%20stream&f=false
> - https://subscription.packtpub.com/book/programming/9781788996242/4/ch04lvl1sec34/the-cuda-callback-function
> - https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#stream-callbacks