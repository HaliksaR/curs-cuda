#include <cuda.h>
#include <iostream>
#include <cmath>

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }

// https://devblogs.nvidia.com/how-optimize-data-transfers-cuda-cc/
enum class To {
    Device,
    Host
};

enum class Transfer {
    Pageable,
    Pinned
};

float transfer(int, To, Transfer);

float transferPageable(int size, To to);

float transferPinned(int size, To to);

float transfer(const int size, const To to, const Transfer transfer) {
    switch (transfer) {
        case Transfer::Pageable:
            return transferPageable(size, to);
        case Transfer::Pinned:
            return transferPinned(size, to);
        default:
            return -1;
    }
}

float transferPinned(const int size, const To to) {
    float *arrayHost, *arrayDevice;
    cudaEvent_t start, stop;
    float elapsedTime = -1;
    CUDA_CHECK(cudaMallocHost(&arrayHost, sizeof(float) * size));
    CUDA_CHECK(cudaMalloc(&arrayDevice, sizeof(float) * size));
    switch (to) {
        case To::Device: {
            CUDA_CHECK(cudaEventCreate(&start));
            CUDA_CHECK(cudaEventCreate(&stop));

            CUDA_CHECK(cudaEventRecord(start, nullptr));
            CUDA_CHECK(cudaMemcpy(arrayDevice, arrayHost, sizeof(float) * size, cudaMemcpyHostToDevice));
            CUDA_CHECK(cudaEventRecord(stop, nullptr));
            CUDA_CHECK(cudaEventSynchronize(stop));

            CUDA_CHECK(cudaEventElapsedTime(&elapsedTime, start, stop));
            break;
        }
        case To::Host: {
            CUDA_CHECK(cudaEventCreate(&start));
            CUDA_CHECK(cudaEventCreate(&stop));

            CUDA_CHECK(cudaEventRecord(start, nullptr));
            CUDA_CHECK(cudaMemcpy(arrayHost, arrayDevice, sizeof(float) * size, cudaMemcpyDeviceToHost));
            CUDA_CHECK(cudaEventRecord(stop, nullptr));
            CUDA_CHECK(cudaEventSynchronize(stop));

            CUDA_CHECK(cudaEventElapsedTime(&elapsedTime, start, stop));
            break;
        }
    }
    CUDA_CHECK(cudaFreeHost(arrayHost));
    CUDA_CHECK(cudaFree(arrayDevice));
    CUDA_CHECK(cudaEventDestroy(start));
    CUDA_CHECK(cudaEventDestroy(stop));
    return elapsedTime;
}

float transferPageable(const int size, const To to) {
    float *arrayHost, *arrayDevice;
    cudaEvent_t start, stop;
    float elapsedTime = -1;

    arrayHost = static_cast<float *>(std::malloc(sizeof(float) * size));
    CUDA_CHECK(cudaMalloc(&arrayDevice, sizeof(float) * size));

    switch (to) {
        case To::Device: {
            CUDA_CHECK(cudaEventCreate(&start));
            CUDA_CHECK(cudaEventCreate(&stop));

            CUDA_CHECK(cudaEventRecord(start, nullptr));
            CUDA_CHECK(cudaMemcpy(arrayDevice, arrayHost, sizeof(float) * size, cudaMemcpyHostToDevice));
            CUDA_CHECK(cudaEventRecord(stop, nullptr));
            CUDA_CHECK(cudaEventSynchronize(stop));

            CUDA_CHECK(cudaEventElapsedTime(&elapsedTime, start, stop));
            break;
        }
        case To::Host: {
            CUDA_CHECK(cudaEventCreate(&start));
            CUDA_CHECK(cudaEventCreate(&stop));

            CUDA_CHECK(cudaEventRecord(start, nullptr));
            CUDA_CHECK(cudaMemcpy(arrayHost, arrayDevice, sizeof(float) * size, cudaMemcpyDeviceToHost));
            CUDA_CHECK(cudaEventRecord(stop, nullptr));
            CUDA_CHECK(cudaEventSynchronize(stop));

            CUDA_CHECK(cudaEventElapsedTime(&elapsedTime, start, stop));
            break;
        }
    }
    free(arrayHost);
    CUDA_CHECK(cudaFree(arrayDevice));
    CUDA_CHECK(cudaEventDestroy(start));
    CUDA_CHECK(cudaEventDestroy(stop));
    return elapsedTime;
}

int main(int argc, char **argv) {
    int size = 22 * pow(2, 11);

    std::cout << "Pageable transfers (malloc) to Device TIME: " <<
              transfer(size, To::Device, Transfer::Pageable)
              << " ms." << std::endl;

    std::cout << "Pageable transfers (malloc) to Host TIME: " <<
              transfer(size, To::Host, Transfer::Pageable)
              << " ms." << std::endl;

    std::cout << std::endl;

    std::cout << "Pageable transfers (cudaMallocHost) to Device TIME: " <<
              transfer(size, To::Device, Transfer::Pinned)
              << " ms." << std::endl;

    std::cout << "Pageable transfers (cudaMallocHost) to Host TIME: " <<
              transfer(size, To::Host, Transfer::Pinned)
              << " ms." << std::endl;

    return 0;
}