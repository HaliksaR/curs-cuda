#include <cuda.h>
#include <cstdio>
#include <sys/time.h>
#include <iostream>
#include <random>

#define N 64
#define DATA_SIZE (N * 10)

//https://github.com/paldir/studia/blob/2eaf7dc203f8c9cb0fa975dbb3fc1c3447839123/10/cuda/LAB9/cuda13/cuda1/cuda13.cu

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }


class Time {
private:
    struct timeval *TStart = nullptr;
    struct timeval *TCurrent = nullptr;
public:
    Time() {
        TStart = static_cast<timeval *>(malloc(sizeof(timeval)));
        TCurrent = static_cast<timeval *>(malloc(sizeof(timeval)));
    }

    ~Time() {
        free(TStart);
        free(TCurrent);
    }

    void start() {
        gettimeofday(TStart, nullptr);
    }

    void stop() {
        gettimeofday(TCurrent, nullptr);
    }

    double get() {
        return static_cast<double>((TCurrent->tv_sec + TCurrent->tv_usec) - (TStart->tv_sec + TStart->tv_usec)) * 1E-6;
    }
};

__global__ void kernel_dgemm(const float *matrixA, const float *matrixB, float *matrixC, const int size) {
    unsigned int x = blockIdx.x + blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y + blockDim.y + threadIdx.y;
    if (y < size && x < size) {
        float sum = 0.0;
        for (int i = 0; i < size; i++)
            sum += matrixA[y * size + i] * matrixB[i * size + x];
        matrixC[y * size + x] = sum;
    }
}

namespace Fill {
    inline void
    CPU_fill_matrix_rand(float *matrix, const int size, const int distributionStart, const int distributionEnd) {
        std::random_device randomDevice{};
        std::mt19937 engine{randomDevice()};
        std::uniform_real_distribution<float> distribution(distributionStart, distributionEnd);
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                matrix[i * size + j] = distribution(engine);
    }
}

void streams(const int streams) {
    auto matrixHostA = static_cast<float *>(std::malloc(DATA_SIZE * DATA_SIZE * sizeof(float)));
    auto matrixHostB = static_cast<float *>(std::malloc(DATA_SIZE * DATA_SIZE * sizeof(float)));
    if (matrixHostA == nullptr || matrixHostB == nullptr)
        exit(EXIT_FAILURE);
    auto matrixHostC = static_cast<float *>(nullptr);

    float *blockDeviceA[streams], *blockDeviceB[streams], *blockDeviceC[streams];

    cudaStream_t stream[streams];
    for (int i = 0; i < streams; i++)
        cudaStreamCreate(&stream[i]);

    Fill::CPU_fill_matrix_rand(matrixHostA, DATA_SIZE, -100, 100);
    Fill::CPU_fill_matrix_rand(matrixHostB, DATA_SIZE, -100, 100);

    CUDA_CHECK(cudaHostAlloc(&matrixHostC, DATA_SIZE * sizeof(float), cudaHostAllocDefault))

    for (int i = 0; i < streams; i++) {
        CUDA_CHECK(cudaMalloc(&blockDeviceA[i], N * sizeof(float)))
        CUDA_CHECK(cudaMalloc(&blockDeviceB[i], N * sizeof(float)))
        CUDA_CHECK(cudaMalloc(&blockDeviceC[i], N * sizeof(float)))
    }

    auto time = new Time();
    time->start();
    for (int i = 0; i < DATA_SIZE; i += N * 2) {
        for (int si = 0; si < streams; si++) {
            CUDA_CHECK(cudaMemcpyAsync(blockDeviceA[si], matrixHostA + i + (N * si), N * sizeof(float), cudaMemcpyHostToDevice, stream[si]))
            CUDA_CHECK(cudaMemcpyAsync(blockDeviceB[si], matrixHostB + i + (N * si), N * sizeof(float), cudaMemcpyHostToDevice, stream[si]))
        }

        for (int si = 0; si < streams; si++)
            kernel_dgemm<<<N/64,64,0,stream[si]>>>(blockDeviceA[si], blockDeviceB[si], blockDeviceC[si], DATA_SIZE);
    }
    for (int si = 0; si < streams; si++)
        CUDA_CHECK(cudaStreamSynchronize(stream[si]))

    time->stop();
    std::cout << "TIME: " << time->get() << "s" << std::endl;

    CUDA_CHECK(cudaFreeHost(matrixHostC))


    for (int i = 0; i < streams; i++) {
        CUDA_CHECK(cudaFree(blockDeviceA[i]))
        CUDA_CHECK(cudaFree(blockDeviceB[i]))
        CUDA_CHECK(cudaFree(blockDeviceC[i]))
    }

    for (int si = 0; si < streams; si++)
        CUDA_CHECK(cudaStreamDestroy(stream[si]))
}

int main() {
    streams(0);
    streams(1);
    streams(2);
    streams(3);
}

