#include <cuda.h>
#include <curand.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <iostream>
#include <vector>
#include <random>

void first();

void second();

void third();

int main() {
    std::cout
            << "Реализуйте функцию fill_rand с использованием библиотеки cuRAND, заполняющую матрицу случайными числами."
            << std::endl;
    first();

    std::cout << std::endl << std::endl
              << "Реализуйте функцию GPU_mmul с использованием библиотеки cuBLAS, вычисляющую произведение двух матриц."
              << std::endl;
    second();

    std::cout << std::endl << std::endl
              << "Реализуйте программу, которая запрашивает размер матриц для умножения, заполняет матрицы случайными числами,\n"
                 "умножает их, находит и выводит минимальный и максимальный элементы матрицы. Матрицы нужно хранить в контейнере\n"
                 "device_vector, минимум и максимум найти средствами библиотеки Thrust." << std::endl;
    third();

    return 0;
}


void GPU_fill_rand(float *pointer, const int rows, const int cols) {
    curandGenerator_t generator;
    curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_XORWOW);
    curandSetPseudoRandomGeneratorSeed(generator, clock());
    curandGenerateUniform(generator, pointer, rows * cols);
}

void
first() {
    int Height = 5;
    int Width = 5;
    int matrixSize = static_cast<int>(Height * Width * sizeof(float));

    auto HostMatrix = static_cast<float *>(std::malloc(matrixSize));
    float *DeviceMatrix = nullptr;
    cudaMalloc(&DeviceMatrix, matrixSize);

    GPU_fill_rand(DeviceMatrix, Height, Width);

    cudaMemcpy(HostMatrix, DeviceMatrix, matrixSize, cudaMemcpyDeviceToHost);
    for (int i = 0; i < Height; i++) {
        for (int j = 0; j < Width; j++) {
            std::cout << "[" << HostMatrix[i * Width + j] << "]";
        }
        std::cout << std::endl;
    }
    free(HostMatrix);
    cudaFree(DeviceMatrix);
}

struct Matrix {
    float *dataHost;
    float *dataDevice;
    int rows, cols;
};


void
GPU_blas_mmul(const float *MatrixA, const float *MatrixB, float *MatrixC, const int rowsA, const int colsA,
              const int colsB) {
    const float alf = 1;
    const float bet = 0;
    const float *alpha = &alf;
    const float *beta = &bet;
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, rowsA, colsB, colsA, alpha, MatrixA, rowsA, MatrixB, colsA, beta,
                MatrixC, rowsA);
    cublasDestroy(handle);
}

void second() {
    Matrix A{}, B{}, C{};
    A.rows = 5;
    A.cols = 5;
    A.dataHost = static_cast<float *>(std::malloc(A.rows * A.cols * sizeof(float)));
    cudaMalloc(&A.dataDevice, A.rows * A.cols * sizeof(float));

    B.rows = 5;
    B.cols = 5;
    B.dataHost = static_cast<float *>(std::malloc(B.rows * B.cols * sizeof(float)));
    cudaMalloc(&B.dataDevice, B.rows * B.cols * sizeof(float));

    C.rows = 5;
    C.cols = 5;
    C.dataHost = static_cast<float *>(std::malloc(C.rows * C.cols * sizeof(float)));
    cudaMalloc(&C.dataDevice, C.rows * C.cols * sizeof(float));

    GPU_fill_rand(A.dataDevice, A.rows, A.cols);
    cudaMemcpy(A.dataHost, A.dataDevice, A.rows * A.cols * sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << std::endl << "МАТРИЦА A" << std::endl;
    for (int i = 0; i < A.cols; i++) {
        for (int j = 0; j < A.rows; j++) {
            std::cout << "[" << A.dataHost[i * A.rows + j] << "]";
        }
        std::cout << std::endl;
    }

    GPU_fill_rand(B.dataDevice, B.rows, B.cols);
    cudaMemcpy(B.dataHost, B.dataDevice, B.rows * B.cols * sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << std::endl << "МАТРИЦА B" << std::endl;
    for (int i = 0; i < B.cols; i++) {
        for (int j = 0; j < B.rows; j++) {
            std::cout << "[" << B.dataHost[i * B.rows + j] << "]";
        }
        std::cout << std::endl;
    }

    GPU_blas_mmul(A.dataDevice, B.dataDevice, C.dataDevice, A.rows, A.cols, B.cols);
    cudaMemcpy(C.dataHost, C.dataDevice, C.rows * C.cols * sizeof(float), cudaMemcpyDeviceToHost);
    std::cout << std::endl << "МАТРИЦА C" << std::endl;
    for (int i = 0; i < C.cols; i++) {
        for (int j = 0; j < C.rows; j++) {
            std::cout << "[" << C.dataHost[i * C.rows + j] << "]";
        }
        std::cout << std::endl;
    }

    cudaFree(A.dataDevice);
    cudaFree(B.dataDevice);
    cudaFree(C.dataDevice);
    free(A.dataHost);
    free(B.dataHost);
    free(C.dataHost);
}


void GPU_find_min_max(thrust::device_vector<float> &DeviceV, float *min, float *max) {
    thrust::pair <thrust::device_vector<float>::iterator, thrust::device_vector<float>::iterator> tuple;
    tuple = thrust::minmax_element(DeviceV.begin(), DeviceV.end());
    *min = *tuple.first;
    *max = *tuple.second;
}

struct Matrix {
    thrust::host_vector<float> dataHost;
    thrust::device_vector<float> dataDevice;
    int col, row;
};

void third() {
    Matrix A{};
    A.row = 5;
    A.col = 5;
    A.dataHost = thrust::host_vector<float>(A.row * A.col);

    std::vector<float> temp;
    std::random_device randomDevice{};
    std::mt19937 engine{randomDevice()};
    std::uniform_real_distribution<float> distribution(0, 5);
    for (int i = 0; i < A.row * A.col; i++)
        temp.push_back(distribution(engine));

    std::cout << std::endl << "МАТРИЦА" << std::endl;
    for (int i = 0; i < A.row; i++) {
        for (int j = 0; j < A.col; j++) {
            std::cout << "[" << temp[i * A.col + j] << "]";
        }
        std::cout << std::endl;
    }

    thrust::copy(temp.begin(), temp.end(), A.dataHost.begin());
    A.dataDevice = A.dataHost;

    float min, max;
    GPU_find_min_max(A.dataDevice, &min, &max);

    std::cout << std::endl << "НАЙДЕНО" << std::endl;
    for (int i = 0; i < A.row; i++) {
        for (int j = 0; j < A.col; j++) {
            if (temp[i * A.col + j] == min) {
                std::cout << "<MIN[" << temp[i * A.col + j] << "]>";
                continue;
            }
            if (temp[i * A.col + j] == max) {
                std::cout << "<MAX[" << temp[i * A.col + j] << "]>";
                continue;
            }
            std::cout << "[" << temp[i * A.col + j] << "]";
        }
        std::cout << std::endl;
    }
}