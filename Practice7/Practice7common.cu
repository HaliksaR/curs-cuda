#include <cuda.h>
#include <curand.h>
#include <cublas_v2.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/transform.h>
#include <thrust/for_each.h>
#include <thrust/device_vector.h>
#include <curand_kernel.h>
#include <iostream>


#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }


template<typename T>
struct Matrix {
    T *dataHost;
    T *dataDevice;
    thrust::device_vector <T> vectorDataDevice;
    int col, row;

    int size() { return col * row * sizeof(T); }

    void initHost() { dataHost = static_cast<float *>(std::malloc(size())); }

    void initDevice() { cudaMalloc(&dataDevice, size()); }

    void initDeviceVector() {
        for (int i = 0; i < row * col; i++)
            vectorDataDevice.push_back(dataHost[i]);
    }

    void memcpyDeviceToHost() {CUDA_CHECK(cudaMemcpy(dataHost, dataDevice, size(), cudaMemcpyDeviceToHost))}

    void print() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++)
                std::cout << "[" << dataHost[i * col + j] << "]";
            std::cout << std::endl;
        }
    }

    ~Matrix() {
        free(dataHost);
        cudaFree(dataDevice);
        vectorDataDevice.clear();
        vectorDataDevice.shrink_to_fit();
    }
};

template<typename T>
void GPU_fill_rand(T *pointer, const int rows, const int cols) {
    curandGenerator_t generator;
    curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_XORWOW);
    curandSetPseudoRandomGeneratorSeed(generator, clock());
    curandGenerateUniform(generator, pointer, rows * cols);
}

template<typename T>
void GPU_find_min_max(thrust::device_vector<T> &DeviceV, T *min, T *max) {
    auto tuple = thrust::minmax_element(DeviceV.begin(), DeviceV.end());
    *min = *tuple.first;
    *max = *tuple.second;
}

template<typename T>
void GPU_mmul(const Matrix<T> *first, const Matrix<T> *second, Matrix<T> *result) {
    const float alpha = 1.0f;
    const float beta = 0.0f;
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, second->col, first->row, first->col, &alpha, second->dataDevice,
                second->col, first->dataDevice, first->col, &beta, result->dataDevice, second->col);
    cublasDestroy(handle);
}

template<typename T>
Matrix<T> *initMatrix(std::string name) {
    auto Matrix = new Matrix<T>();
    std::cout << "Enter Matrix " << name << " row & col : ";
    std::cin >> Matrix->row >> Matrix->col;
    Matrix->initHost();
    Matrix->initDevice();
    GPU_fill_rand<T>(Matrix->dataDevice, Matrix->row, Matrix->col);
    Matrix->memcpyDeviceToHost();
    std::cout << "Matrix: " << name << std::endl;
    Matrix->print();
    return Matrix;
}

template<typename T>
Matrix<T> *initResultMatrix(const Matrix<T> *m1, const Matrix<T> *m2) {
    auto Matrix = new Matrix<T>();
    if (m2->row == m1->col) {
        Matrix->row = m1->row;
        Matrix->col = m2->col;
    } else {
        delete Matrix;
        return nullptr;
    }
    Matrix->initHost();
    Matrix->initDevice();
    return Matrix;
}

template<typename T>
void printMinMax(const Matrix<T> *C, const T min, const T max) {
    for (int i = 0; i < C->row; i++) {
        for (int j = 0; j < C->col; j++) {
            if (C->dataHost[i * C->col + j] == min) {
                std::cout << "<MIN[" << C->dataHost[i * C->col + j] << "]>";
                continue;
            }
            if (C->dataHost[i * C->col + j] == max) {
                std::cout << "<MAX[" << C->dataHost[i * C->col + j] << "]>";
                continue;
            }
            std::cout << "[" << C->dataHost[i * C->col + j] << "]";
        }
        std::cout << std::endl;
    }
}
template<typename T>
void printMinMaxSimple(const T min, const T max) {
    std::cout << "MIN=" << min <<", MAX=" << max << std::endl;
}

int main(int argc, char *argv[]) {
    auto A = initMatrix<float>("A");

    auto B = initMatrix<float>("B");

    auto C = initResultMatrix<float>(A, B);

    if (C == nullptr) {
        delete A;
        delete B;
        exit(EXIT_FAILURE);
    }

    std::cout << "Calculate.." << std::endl;
    GPU_mmul<float>(A, B, C);

    C->memcpyDeviceToHost();
    std::cout << "Done!" << std::endl;

#ifdef ENABLE_DEBUG_MACRO
    std::cout << "Result: " << std::endl;
    C->print();
#endif

    std::cout << "Search MIN MAX..." << std::endl;
    C->initDeviceVector();

    float min, max;
    GPU_find_min_max<float>(C->vectorDataDevice, &min, &max);

    std::cout << "Find!" << std::endl;

#ifdef ENABLE_DEBUG_MACRO
    printMinMax<float>(C, min, max);
#endif

    printMinMaxSimple(min, max);

    delete A;
    delete B;
    delete C;
}