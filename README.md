# Курс Cuda
### Utils
```sh
$ watch -n 0.5 nvidia-smi
$ nvidia-settings
```
### Names projects
#### Base
- lab1
- lab2

#### Practice
- Practice2
- Practice3
  - Practice3Gauss
  - Practice3Iter
- Practice4
- Practice5
- Practice6

### Build commands
```sh
$ ./Build.sh <candidate> <command?> <program?> <args_program?> 
```
candidate
- --Debug || -D     | инициализация cmake с отладочной информацией
- --Release || -R   | инициализация cmake с отладочной информацией

command
- --build || -b     | билд проекта
- --run || -r       | запуск проекта
- --clean || -c     | очистка билда