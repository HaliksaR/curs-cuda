#include <cuda.h>
#include <cstdio>

__global__ void gTest(const float *a, const float *b, float *c);

int main() {
    float *a, *aCuda, *b, *bCuda, *c, *cCuda;
    const int num_of_blocks = 30;
    const int threads_per_block = 40;
    int N = num_of_blocks * threads_per_block;
    a = (float *) calloc(N, sizeof(float));
    cudaMalloc((void **) &aCuda, N * sizeof(float));
    for (size_t i = 0; i < N; i++) {
        a[i] = i * 20;
    }
    cudaMemcpy(aCuda, a, N * sizeof(float), cudaMemcpyHostToDevice);

    b = (float *) calloc(N, sizeof(float));
    cudaMalloc((void **) &bCuda, N * sizeof(float));
    for (size_t i = 0; i < N; i++) {
        b[i] = i * 46;
    }
    cudaMemcpy(bCuda, b, N * sizeof(float), cudaMemcpyHostToDevice);

    c = (float *) calloc(N, sizeof(float));
    cudaMalloc((void **) &cCuda, N * sizeof(float));

    gTest<<<dim3(num_of_blocks), dim3(threads_per_block)>>>(aCuda, bCuda, cCuda);
    cudaDeviceSynchronize();

    cudaMemcpy(c, cCuda, N * sizeof(float), cudaMemcpyDeviceToHost);

    for (size_t i = 0; i < N; i++) {
        printf("%f \n", c[i]);
    }

    free(a);
    free(b);
    free(c);

    cudaFree(aCuda);
    cudaFree(bCuda);
    cudaFree(cCuda);
    return 0;
}

__global__ void gTest(const float *a, const float *b, float *c) {
    unsigned int i = threadIdx.x + blockDim.x * blockIdx.x;
    c[i] = (float) (a[i] + b[i]);
}