#include <cuda.h>
#include <cstdio>


#define CUDA_CHECK_RETURN(value) {                                            \
    cudaError_t _m_cudaStat = value;                                        \
    if (_m_cudaStat != cudaSuccess) {                                        \
        fprintf(stderr, "Error %s at line %d in file %s\n",                    \
                cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);        \
        exit(1);                                                            \
    } }

__global__ void gTest(const float *a, const float *b, float *c) {
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    c[i] = (float) (a[i] + b[i]);
}

int main() {
    float *a, *aCuda, *b, *bCuda, *c, *cCuda;
    const int num_of_blocks = 4900;
    const int threads_per_block = 1000;
    int N = num_of_blocks * threads_per_block;
    a = (float *) calloc(N, sizeof(float));
    for (size_t i = 0; i < N; i++) {
        a[i] = i * 20;
    }
    b = (float *) calloc(N, sizeof(float));
    for (size_t i = 0; i < N; i++) {
        b[i] = i * 46;
    }
    c = (float *) calloc(N, sizeof(float));

    cudaEvent_t start, stop;
    float elapsedTime;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start, NULL);

    CUDA_CHECK_RETURN(cudaMalloc((void **) &aCuda, N * sizeof(float)))
    CUDA_CHECK_RETURN(cudaMemcpy(aCuda, a, N * sizeof(float), cudaMemcpyHostToDevice));


    CUDA_CHECK_RETURN(cudaMalloc((void **) &bCuda, N * sizeof(float)))
    CUDA_CHECK_RETURN(cudaMemcpy(bCuda, b, N * sizeof(float), cudaMemcpyHostToDevice));

    CUDA_CHECK_RETURN(cudaMalloc((void **) &cCuda, N * sizeof(float)))

    gTest<<<dim3(num_of_blocks), dim3(threads_per_block)>>>(aCuda, bCuda, cCuda);
    CUDA_CHECK_RETURN(cudaDeviceSynchronize())
    CUDA_CHECK_RETURN(cudaGetLastError())

    CUDA_CHECK_RETURN(cudaMemcpy(c, cCuda, N * sizeof(float), cudaMemcpyDeviceToHost));

    cudaEventRecord(stop, NULL);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);

    fprintf(stderr, "gTest took %g\n", elapsedTime);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    /*  for (size_t i = 0; i < N; i++) {
          printf("%f ", c[i]);
      }*/

    free(a);
    free(b);
    free(c);

    cudaFree(aCuda);
    cudaFree(bCuda);
    cudaFree(cCuda);
    return 0;
}