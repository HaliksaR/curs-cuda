#pragma once

#include <utility>
#include <vector>


#define BLOCK_SIZE 32

struct Cords {
    double x;
    double y;
};

struct Limits {
    double min;
    double max;
};

struct Config {
    int count_of_scout_bees = 20;
    int count_of_best_areas = 2;
    int count_of_perspective_areas = 5;
    int count_of_bees_to_best_area = 10;
    int count_of_bees_to_perspective_area = 2;
    Limits *limits = nullptr;

    double (*fitness)(const Cords *) = nullptr;

    double neighbourhood_value = 0.5;
    int max_iterations = 10;

    int count_of_bees() const {
        return count_of_scout_bees
               + count_of_bees_to_best_area * count_of_best_areas
               + count_of_bees_to_perspective_area * count_of_perspective_areas;
    }
};

struct Bee {
    Cords *cords;
    double value;
};