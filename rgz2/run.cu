#include <iostream>
#include "ABC.cuh"
#include <curand.h>
#include <cuda.h>
#include <curand_kernel.h>

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }


double fitness(const Cords *cords) {
    return 100
           * (cords->x - cords->y * cords->y)
           * (cords->x - cords->y * cords->y)
           + (cords->x - 1)
             * (cords->x - 1);
}

__global__ void gen_cords_all_bees_init(unsigned int seed, curandState_t *states) {
    curand_init(seed, blockIdx.x, 0, &states[blockIdx.x]);
}

__device__ double randoms(curandState_t state, double min, double max) {
    return fmod(static_cast<float>(curand(&state)), static_cast<float>((max - min + 1) + min));
}

__global__ void
gen_cords_all_bees_randoms(Bee *bees, Bee *bee, Cords *cordss, Cords *cords, Config *config, curandState_t *states) {
    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    for (int i = 0; i < sizeof(config->limits) / sizeof(struct Limits); i++) {
        cords->x = randoms(states[blockIdx.x], config->limits[i].min, config->limits[i].max);
        cords->y = randoms(states[blockIdx.x], config->limits[i].min, config->limits[i].max);
        cordss[i] = *cords;
    }
    bee->cords = cordss;
    bee->value = config->fitness(cords);
    bees[x] = *bee;
}

void abc(const Config *config, Cords *vector) {
    int count_of_bees = config->count_of_bees();

    Bee *beesHost = static_cast<Bee *>(std::malloc(count_of_bees * sizeof(Bee)));
    Bee *beesDevice;
    CUDA_CHECK(cudaMalloc(&beesDevice, count_of_bees * sizeof(struct Bee)))

    Bee *beeDevice;
    CUDA_CHECK(cudaMalloc(&beeDevice, sizeof(struct Bee)))

    Cords *cordsDevice;
    CUDA_CHECK(cudaMalloc(&cordsDevice, sizeof(struct Cords)))

    Cords *cordssDevice;
    cudaMalloc(&cordssDevice, (sizeof(config->limits) / sizeof(struct Limits)) * sizeof(struct Cords));

    Config *confDevice;
    cudaMalloc(&confDevice, sizeof(struct Config));
    cudaMemcpy(confDevice, config, sizeof(struct Config), cudaMemcpyHostToDevice);

    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid((count_of_bees + BLOCK_SIZE - 1) / BLOCK_SIZE, (count_of_bees + BLOCK_SIZE - 1) / BLOCK_SIZE);

    curandState_t *states;
    cudaMalloc(&states, count_of_bees * sizeof(curandState_t));

    gen_cords_all_bees_init<<<dimGrid, dimBlock>>>(time(NULL), states);
    gen_cords_all_bees_randoms<<<dimGrid, dimBlock>>>(beesDevice, beeDevice, cordssDevice, cordsDevice, confDevice,
                                                      states);
}


int main() {
    Config config;

    auto limits = static_cast<Limits *>(std::malloc(2 * sizeof(struct Limits)));
    limits[0] = Limits{-10, 10};
    limits[2] = Limits{-10, 10};
    config.limits = limits;

    config.fitness = fitness;
    config.max_iterations = 500;

    auto best = static_cast<Cords *>(std::malloc(sizeof(struct Cords)));

    abc(&config, best);

    std::cout << "x = " << best->x << std::endl
              << "y = " << best->y << std::endl;

    return 0;
}