#include <cuda.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <ctime>
#include <exception>
#include <iostream>
#include <functional>
#include <vector>
#include <random>

#define IDX2C(i, j, ld) (((j) * (ld)) + (i))
#define BLOCK_SIZE 16

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }


struct Matrix {
    float *dataHost;
    float *dataDevice;
    int col, row;

    int size() { return col * row * sizeof(float); }

    int length() { return col * row; }

    void mallocHost() { dataHost = static_cast<float *>(std::malloc(size())); }

    void mallocDevice() { cudaMalloc(&dataDevice, size()); }

    void memcpyDeviceToHost() {CUDA_CHECK(cudaMemcpy(dataHost, dataDevice, size(), cudaMemcpyDeviceToHost))}

    void memcpyHostToDevice() {CUDA_CHECK(cudaMemcpy(dataDevice, dataHost, size(), cudaMemcpyHostToDevice))}

    void callocHost() { dataHost = static_cast<float *>(std::calloc(col * row, size())); }

    void fillRand() {
        std::random_device random;
        std::mt19937 mt(random());
        std::uniform_real_distribution<float> dist(-10.0, 10.0);
        for (int i = 0; i < col * row; i++)
            dataHost[i] = dist(mt);
    }

    void printMatrix() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++)
                std::cout << "[" << dataHost[IDX2C(i, j, row)] << "]";
            std::cout << std::endl;
        }
        std::cout << std::endl << std::endl;
    }

    void freeDevice() { cudaFree(dataDevice); }

    void freeHost() { free(dataHost); }

    ~Matrix() {
        freeDevice();
        freeHost();
    }
};

typedef std::pair<std::string, std::function<void(Matrix *, Matrix *, Matrix *)>> func;
typedef std::vector<func> funcs;

__global__ void matrix_mult(float *a, float *b, float *c, int m, int n, int k) {
    unsigned row = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0;
    if (col < k && row < m) {
        for (int i = 0; i < n; i++)
            sum += a[row * n + i] * b[i * k + col];
        c[row * k + col] = sum;
    }
}

void basicCuda(Matrix *A, Matrix *B, Matrix *R) {
    std::cout << "basicCuda" << std::endl;
    A->mallocDevice();
    A->memcpyHostToDevice();

    B->mallocDevice();
    B->memcpyHostToDevice();

    R->mallocDevice();

    unsigned int grid_rows = (A->col + BLOCK_SIZE - 1) / BLOCK_SIZE;
    unsigned int grid_cols = (B->row + BLOCK_SIZE - 1) / BLOCK_SIZE;
    dim3 dimGrid(grid_cols, grid_rows);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    matrix_mult<<<dimGrid, dimBlock>>>(A->dataDevice, B->dataDevice, R->dataDevice, A->col, A->row, B->row);
    R->memcpyDeviceToHost();

    A->freeDevice();
    B->freeDevice();
    R->freeDevice();
}

void thrustCuda(Matrix *A, Matrix *B, Matrix *R) {
    std::cout << "thrustCuda" << std::endl;
    thrust::device_vector<float> Amd(A->dataHost, A->dataHost + A->length());
    thrust::device_vector<float> AmLd(A->col);
    thrust::device_vector<float> AmTd(A->row);
    thrust::device_vector<float> Bmd(B->dataHost, B->dataHost + B->length());
    thrust::device_vector<float> BmCd(B->col);

    for (int i = 0; i < A->col; ++i) {
        for (int j = 0; j < A->row; ++j) {
            float reduce = 0.0f;
            for (int k = 0; k < A->col; k++)
                AmLd[k] = Amd[IDX2C(j, k, A->row)];
            for (int k = 0; k < B->row; k++)
                BmCd[k] = Bmd[IDX2C(k, j, B->row)];
            thrust::transform(
                    AmLd.begin(), AmLd.end(),
                    BmCd.begin(),
                    AmTd.begin(),
                    thrust::multiplies<float>()
            );
            for (int k = 0; k < R->row; ++k)
                reduce = thrust::reduce(AmTd.begin(), AmTd.end());
            R->dataHost[IDX2C(i, j, A->row)] = reduce;
        }
    }
}

void blasCuda(Matrix *A, Matrix *B, Matrix *R) {
    std::cout << "blasCuda" << std::endl;
    A->mallocDevice();
    A->memcpyHostToDevice();

    B->mallocDevice();
    B->memcpyHostToDevice();

    R->mallocDevice();

    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasSetMatrix(A->row, A->col, sizeof(float), A->dataHost, A->row, A->dataDevice, A->col);
    cublasSetMatrix(B->row, B->col, sizeof(float), B->dataHost, B->row, B->dataDevice, B->col);
    cublasSetMatrix(R->row, R->col, sizeof(float), R->dataHost, R->row, R->dataDevice, R->col);

    float al = 1.0f;
    float bet = 1.0f;

    cublasSgemm(
            handle,
            CUBLAS_OP_N, CUBLAS_OP_N,
            A->col, B->row, B->col, &al, A->dataDevice, A->col, B->dataDevice, B->col, &bet, R->dataDevice, R->col
    );

    cublasGetMatrix(R->row, R->col, sizeof(float), R->dataHost, R->row, R->dataDevice, R->col);

    A->freeDevice();
    B->freeDevice();
    R->freeDevice();
    cublasDestroy(handle);
}


int main(int argc, char *argv[]) {
    auto A = new Matrix();
    auto B = new Matrix();
    auto R = new Matrix();

    try {
        A->col = R->col = std::stoi(argv[1]);
        A->row = B->col = std::stoi(argv[2]);
        B->row = R->row = std::stoi(argv[3]);
    } catch (const std::exception &e) {
        std::cout << "INVALID ARGS" << std::endl;
        exit(EXIT_FAILURE);
    }

    A->mallocHost();
    A->fillRand();
    B->mallocHost();
    B->fillRand();
    R->callocHost();

    funcs funcs = {
            func("CUDA", basicCuda),
            func("THRUST", thrustCuda),
            func("BLAS", blasCuda)
    };

    for (auto const &func : funcs) {
        unsigned int start_time = clock();
        func.second(A, B, R);
        unsigned int end_time = clock();

        float search_time = (end_time - start_time) / 1000.0;
        std::cout << func.first << " TIME: " << search_time << " ms." << std::endl;

        R->callocHost();
    }

    delete A;
    delete B;
    delete R;
}