#include <cuda.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <ctime>
#include <exception>
#include <iostream>
#include <functional>
#include <vector>
#include <random>

#define IDX2C(i, j, ld) (((j) * (ld)) + (i))

#define CUDA_CHECK(value) {\
 cudaError_t _m_cudaStat = value;\
     if(_m_cudaStat != cudaSuccess) {\
         fprintf(stderr, "Error %s at line %d in file %s\n", cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);\
         exit(1);\
     }\
 }

typedef std::pair<std::string, std::function<void(const float *, const float *, float *, const int, const int)>> func;
typedef std::vector<func> funcs;

void fillRand(float *pointer, const int count) {
    std::random_device random;
    std::mt19937 mt(random());
    std::uniform_real_distribution<float> dist(-10.0, 10.0);
    for (int i = 0; i < count; i++)
        pointer[i] = dist(mt);
}

void printVector(const float *v, int count) {
    for (int i = 0; i < count; i++)
        std::cout << "[" << v[i] << "]" << std::endl;
    std::cout << std::endl << std::endl;
}

void printMatrix(const float *m, const int cols, const int rows) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            std::cout << "[" << m[IDX2C(i, j, rows)] << "]";
        std::cout << std::endl;
    }
    std::cout << std::endl << std::endl;
}

__global__ void kernel(const float *AmD, const float *AvD, float *RvD, const int cols, const int rows) {
    unsigned x = blockIdx.x * blockDim.x + threadIdx.x;
    RvD[x] = 0;
    for (int j = 0; j < cols; j++)
        RvD[x] += AmD[IDX2C(x, j, rows)] * AvD[j];
}

void basicCuda(const float *Amh, const float *Avh, float *Rvh, const int cols, const int rows) {
    float *AmD, *AvD, *RvD;
    cudaMalloc(&AmD, cols * rows * sizeof(float));
    cudaMemcpy(AmD, Amh, cols * rows * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&AvD, cols * sizeof(float));
    cudaMemcpy(AvD, Avh, cols * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&RvD, rows * sizeof(float));

    dim3 threads(rows, 1, 1);
    dim3 grid(1, 1, 1);
    kernel<<<grid, threads>>>(AmD, AvD, RvD, cols, rows);
    cudaDeviceSynchronize();

    cudaMemcpy(Rvh, RvD, rows * sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(AmD);
    cudaFree(AvD);
    cudaFree(RvD);
}

void thrustCuda(const float *Amh, const float *Avh, float *Rvh, const int cols, const int rows) {
    thrust::device_vector<float> Amd(Amh, Amh + (cols * rows));
    thrust::device_vector<float> Atld(cols);
    thrust::device_vector<float> Avd(Avh, Avh + cols);
    thrust::device_vector<float> Atmd(cols);
    float reduce = 0.0f;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++)
            Atld[j] = Amd[IDX2C(i, j, rows)];

        thrust::transform(
                Atld.begin(), Atld.end(),
                Avd.begin(),
                Atmd.begin(),
                thrust::multiplies<float>()
        );

        for (int j = 0; j < cols; j++)
            reduce = thrust::reduce(Atmd.begin(), Atmd.end());

        Rvh[i] = reduce;
        reduce = 0.0f;
    }
}

void blasCuda(const float *Amh, const float *Avh, float *Rvh, const int cols, const int rows) {
    float *AmD, *AvD, *RvD;
    cudaMalloc(&AmD, cols * rows * sizeof(float));
    cudaMemcpy(AmD, Amh, cols * rows * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&AvD, cols * sizeof(float));
    cudaMemcpy(AvD, Avh, cols * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&RvD, rows * sizeof(float));

    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasSetMatrix(rows, cols, sizeof(float), Amh, rows, AmD, rows);
    cublasSetVector(cols, sizeof(float), Avh, 1, AvD, 1);
    cublasSetVector(rows, sizeof(float), Rvh, 1, RvD, 1);
    float al = 1.0f;
    float bet = 1.0f;

    cublasSgemv(handle, CUBLAS_OP_N, rows, cols, &al, AmD, rows, AvD, 1, &bet, RvD, 1);
    cublasGetVector(rows, sizeof(float), RvD, 1, Rvh, 1);

    cudaFree(AmD);
    cudaFree(AvD);
    cudaFree(RvD);
    cublasDestroy(handle);
}


int main(int argc, char *argv[]) {
    int cols, rows;
    try {
        cols = std::stoi(argv[1]);
        rows = std::stoi(argv[2]);
    } catch (const std::exception &e) {
        std::cout << "INVALID ARGS" << std::endl;
        exit(EXIT_FAILURE);
    }

    float *Amh, *Avh, *Rvh;
    Amh = static_cast<float *>(std::malloc(cols * rows * sizeof(float)));
    Avh = static_cast<float *>(std::malloc(cols * sizeof(float)));
    Rvh = static_cast<float *>(std::calloc(rows, sizeof(float)));

    fillRand(Amh, cols * rows);
//    printMatrix(Amh, cols, rows);

    fillRand(Avh, cols);
//    printVector(Avh, cols);

    funcs funcs = {
            func("CUDA", basicCuda),
            func("THRUST", thrustCuda),
            func("BLAS", blasCuda)
    };

    std::cout << "[" << cols << "x" << rows << "]\t" << rows*cols << "\t";
    for (auto const &func : funcs) {
        unsigned int start_time = clock();
        func.second(Amh, Avh, Rvh, cols, rows);
        unsigned int end_time = clock();

        float search_time = (end_time - start_time) / (double) CLOCKS_PER_SEC;
        std::cout << "\t" << search_time;

//        printVector(Rvh, rows);
        Rvh = static_cast<float *>(std::calloc(rows, sizeof(float)));
    }
    std::cout << std::endl;
    free(Amh);
    free(Avh);
    free(Rvh);
}