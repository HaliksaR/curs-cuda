#include <cuda.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <ctime>
#include <exception>
#include <iostream>
#include <functional>
#include <vector>
#include <random>

#define BLOCK_SIZE 32

typedef std::pair<std::string, std::function<void(const float *, float *, const int, const float)>> func;
typedef std::vector<func> funcs;

void fillRand(float *pointer, const int count) {
    std::random_device random;
    std::mt19937 mt(random());
    std::uniform_real_distribution<float> dist(-10.0, 10.0);
    for (int i = 0; i < count; i++)
        pointer[i] = dist(mt);
}

__global__ void saxpy(int n, float a, float *x, float *y) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) y[i] = a * x[i] + y[i];ну
}

void basicCuda(const float *xh, float *yh, const int count, const float scalar) {
    float *xd, *yd;
    cudaMalloc(&xd, count * sizeof(float));
    cudaMemcpy(xd, xh, count * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&yd, count * sizeof(float));
    cudaMemcpy(yd, yh, count * sizeof(float), cudaMemcpyHostToDevice);

    unsigned int grid = (count + BLOCK_SIZE - 1) / BLOCK_SIZE;
    dim3 dimGrid(grid, 1);
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    saxpy<<<dimGrid, dimBlock>>>(count, scalar, xd, yd);

    cudaMemcpy(yh, yd, count, cudaMemcpyDeviceToHost);
    cudaFree(xd);
    cudaFree(yd);
}


void thrustCuda(const float *xh, float *yh, const int count, const float scalar) {
    thrust::device_vector<float> xd(xh, xh + count);
    thrust::device_vector<float> yd(yh, yh + count);
    thrust::device_vector<float> temp(count);

    // temp <- A
    thrust::fill(
            temp.begin(), temp.end(),
            scalar
    );

    // temp <- A * X
    thrust::transform(
            xd.begin(), xd.end(),
            temp.begin(),
            temp.begin(),
            thrust::multiplies<float>()
    );

    // Y <- A * X + Y
    thrust::transform(
            temp.begin(), temp.end(),
            yd.begin(),
            yd.begin(),
            thrust::plus<float>()
    );

    yh = thrust::raw_pointer_cast(&yd[0]);
}

void blasCuda(const float *xh, float *yh, const int count, const float scalar) {
    float *xd, *yd;
    cudaMalloc(&xd, count * sizeof(float));
    cudaMemcpy(xd, xh, count * sizeof(float), cudaMemcpyHostToDevice);

    cudaMalloc(&yd, count * sizeof(float));
    cudaMemcpy(yd, yh, count * sizeof(float), cudaMemcpyHostToDevice);

    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasSetVector(count, sizeof(float), xh, 1, xd, 1);
    cublasSetVector(count, sizeof(float), yh, 1, yd, 1);

    cublasSaxpy(
            handle,
            count,
            &scalar,
            xd, 1,
            yd, 1
    );

    cublasGetVector(count, sizeof(float), yd, 1, yh, 1);
    cublasDestroy(handle);
}


int main(int argc, char *argv[]) {
    int count;
    float scalar;
    try {
        count = std::stoi(argv[1]);
        scalar = std::stof(argv[2]);
    } catch (const std::exception &e) {
        std::cout << "INVALID ARGS" << std::endl;
        exit(EXIT_FAILURE);
    }

    float *xh, *yh;
    xh = static_cast<float *>(std::malloc(count * sizeof(float)));
    yh = static_cast<float *>(std::malloc(count * sizeof(float)));

    fillRand(xh, count);
    fillRand(yh, count);

    funcs funcs = {
            func("CUDA", basicCuda),
            func("THRUST", thrustCuda),
            func("BLAS", blasCuda)
    };

    std::cout << count << "\t";
    for (auto const &func : funcs) {
        unsigned int start_time = clock();
        func.second(xh, yh, count, scalar);
        unsigned int end_time = clock();

        float search_time = (end_time - start_time) / (double) CLOCKS_PER_SEC;
        std::cout << "\t" << search_time;
    }
    std::cout << std::endl;
    free(xh);
    free(yh);
}